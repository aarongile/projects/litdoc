;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2024 Aron Gile <aronggile@gmail.com>


(use-modules (nongnu packages please-build))
(specifications->manifest
 '(
   ;; minimal shell
   "bash"
   "xterm"
   "ncurses"
   ;; basic shell commands
   "util-linux"
   "coreutils"
   "grep"
   "sed"
   "gawk"
   "tree"
   "which"
   "findutils"
   "iputils"
   "curl"
   "gzip"
   "bc"
   ;; dev-tools
   "please-build"
   "clang-toolchain@19.1"
   "make"
   "git"
   "lld"
   "node"
   "vim"
   ;; libs
   "nss-certs"
   "openssl"
   "gnutls"
   ;;"glibc-locales"
   "linux-libre-headers"
   ;;tree-sitter
   "tree-sitter"
   "tree-sitter-cli"
   ;;"tree-sitter-org"
   ;;C/C++
   "fmt@11.1.4"
   "spdlog@1.13.0"
   ;; Guile
   "guile"
   ;; test
   "doctest"
   ))
