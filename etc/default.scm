;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2024 Aron Gile <aronggile@gmail.com>


(specifications->manifest
 '(;; shell
   "bash"

   ;; editor
   "vim"

   ;; core
   "coreutils"
   "util-linux"
   "tree"
   "which"
   "htop"

   ;; history
   "hstr"


   ;; text based UI programming
   "ncurses"

   ;; networking
   "iputils"
   "gnutls"
   "openssl"

   ;; text processing
   "jq"
   "grep"
   "sed"

   ;; file & directory search
   "findutils"
   "the-silver-searcher"))
