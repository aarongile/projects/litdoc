;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2024 Aron Gile <aronggile@gmail.com>

(use-modules (nongnu packages please-build))
(specifications->manifest
 '(
   "tree-sitter"
   "tree-sitter-org"
   "spdlog"
   "fmt"
   "gzip"
   "openssl"
   ))
