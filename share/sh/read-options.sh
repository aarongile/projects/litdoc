# -*- coding: utf-8; mode: shell-script -*-
### SPDX-License-Identifier: GPL-3.0-or-later
### Copyright © 2025 Aron Gile <aronggile@gmail.com>


#==============================================================================
# OPTIONS
#==============================================================================
set   -e
set   -o pipefail

function read_options( )  {
    # Read shell command options from FILE.
    #
    # ARGS:
    # - FILE($1): command options file path
    #
    # Output:
    # FILE content is written to stdout after
    # striping comment lines and expanding any
    # environment variables
    #
    # RETURNS
    # zero on success otherwise nonzero

    local options_file=$1; shift
    [ -f "${options_file}" ] && {
        # strip comments, substitute variables,
        # and write output to stdout
        grep -o '^[^#]*' ${options_file} | envsubst | tee -a || return 1
    }

    return 0
}
