# -*- coding: utf-8; mode: shell-script -*-
### SPDX-License-Identifier: GPL-3.0-or-later
### Copyright © 2024 Aron Gile <aronggile@gmail.com>

# set log level"
: ${LITDOC_LOGLEVEL:=error}
[ -n ${LITDOC_LOGFILE} ] && LITDOC_COLOR_DISABLED=true
.  "${LITDOC_DIR}/share/sh/log-color.sh"


case ${LITDOC_LOGLEVEL} in
    info)
        export stdout="${LITDOC_LOGFILE:-/dev/stdout}"
        export stderr="${LITDOC_LOGFILE:-/dev/stderr}"
        export stdwarn="${LITDOC_LOGFILE:-/dev/stderr}"
        ;;
    warn)
        export stdout="/dev/null"
        export stdwarn="${LITDOC_LOGFILE:-/dev/stderr}"
        export stderr="${LITDOC_LOGFILE:-/dev/stderr}"
        ;;
    error)
        export stdout="/dev/null"
        export stdwarn="/dev/null"
        export stderr="${LITDOC_LOGFILE:-/dev/stderr}"
        ;;
    quite)
        export stdout="/dev/null"
        export stdwarn="/dev/null"
        export stderr="/dev/null"
        ;;
    *)
        echo "[ERROR]: unknown log level: LITDOC_LOGLEVEL=${LITDOC_LOGLEVEL}"
        exit 1
	    ;;
esac

log_info() {
    echo "${LITDOC_COLOR_INFO}==> ${@}${LITDOC_COLOR_NONE}" > ${stdout}
}

log_subinfo() {
    echo "${LITDOC_COLOR_INFO}[INFO]: ${@}${LITDOC_COLOR_NONE}" > ${stdout}
}

log_error() {
    echo "${LITDOC_COLOR_ERROR}[ERROR]: ${@}${LITDOC_COLOR_NONE}" > ${stderr}
}

log_warn() {
    echo "${LITDOC_COLOR_WARN}[WARN]: ${@}${LITDOC_COLOR_NONE}" > ${stdwarn}
}

log_msg() {
    local key="$1"; shift
    local value="${@}"
    echo "${LITDOC_COLOR_HINT}${key}${LITDOC_COLOR_NONE} : ${LITDOC_COLOR_HINT}${value}${LITDOC_COLOR_NONE}"
}

log_fail() {
    log_error "${1:-unknown error}"
    exit 1
}
