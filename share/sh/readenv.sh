# -*- coding: utf-8; mode: shell-script -*-
### SPDX-License-Identifier: GPL-3.0-or-later
### Copyright © 2025 Aron Gile <aronggile@gmail.com>

readenv ( ) {

    # Read environment variables from FILE($1) or stdin and expand it inline.
    #
    # The function also strips export keyword and expands environment
    # variables. This is useful when exporting environment variables
    # using env command
    #
    # Usage:
    # variables=$(cat variables.env | readenv)
    # env ${variables} ...

    local _input=${1:-$(</dev/stdin)};
    echo "${_input}" | grep -v '^#' | sed -e 's/\<export\>//g' | xargs -d$'\n' echo || {
        return 1
    }

    return 0
}
