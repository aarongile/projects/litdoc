# -*- coding: utf-8; mode: shell-script -*-
### SPDX-License-Identifier: GPL-3.0-or-later
### Copyright © 2025 Aron Gile <aronggile@gmail.com>

#########################################################
# Color
# Manage color of terminal output using tput
#########################################################

color_is_enabled() {
    # is tput available?
    command -v tput   2>&1 > /dev/null  || return -1
    [ -n "${LITDOC_COLOR_DISABLED}" ]   && return -1

    return 0
}

# check tput support or turn-off color
if color_is_enabled; then
    export LITDOC_COLOR_NONE="$(tput  sgr0)"                              # default
    export LITDOC_COLOR_BOLD="$(tput  bold)"                              # bold
    export LITDOC_COLOR_INFO="$(tput  setaf 3)"                           # yellow
    export LITDOC_COLOR_HINT="$(tput  setaf 7)"                           # bright white
    export LITDOC_COLOR_WARN="${LITDOC_COLOR_BOLD}${LITDOC_COLOR_INFO}"   # dark yellow
    export LITDOC_COLOR_ERROR="$(tput setaf 1)"                           # red
else
    export LITDOC_COLOR_NONE=""
    export LITDOC_COLOR_BOLD=""
    export LITDOC_COLOR_INFO=""
    export LITDOC_COLOR_HINT=""
    export LITDOC_COLOR_WARN=""
    export LITDOC_COLOR_ERROR=""
fi
