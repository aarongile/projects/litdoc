;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2025 Aron Gile <aronggile@gmail.com>


(define-module (guix extensions dispatch)
  #:use-module (guix ui)
  #:use-module ((guix diagnostics) #:select (location))
  #:use-module (guix scripts package)
  #:use-module (guix scripts shell)
  #:autoload   (guix scripts build) (show-build-options-help)
  #:autoload   (guix transformations) (show-transformation-options-help)
  #:use-module (guix scripts)
  #:use-module (guix packages)
  #:use-module (guix profiles)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-37)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:autoload   (ice-9 rdelim) (read-line)
  #:autoload   (guix base32) (bytevector->base32-string)
  #:autoload   (guix utils) (config-directory cache-directory)
  #:autoload   (guix describe) (current-channels)
  #:autoload   (guix channels) (channel-commit)
  #:autoload   (gcrypt hash) (sha256)
  #:use-module ((guix build utils) #:select (mkdir-p))
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (guix cache)
  #:use-module ((ice-9 ftw) #:select (scandir))
  #:autoload   (ice-9 pretty-print) (pretty-print)
  #:autoload   (gnu packages) (cache-is-authoritative?
                               package-unique-version-prefix
                               specification->package
                               specification->package+output
                               specifications->manifest)
  #:export (dispatch-init-module
            dispatch-version
            dispatch-find
            dispatch-lsdeps
            dispatch-build
            dispatch-run))
;;; utils
(define (stdout->string thunk)
  (let* ((pipe (pipe)) ;; Create (read-fd . write-fd)
         (read-end   (car pipe))
         (write-end  (cdr pipe))
         (stdout (dup 1))) ;; Save original stdout
    (dup2 (port->fdes write-end) 1)
    (close write-end)
    (thunk)
    (dup2 stdout 1)
    (close stdout)
    (let* ((port   (fdopen (port->fdes read-end) "r"))
           (output (read-string port)))
      (close-port port)
      output)))

;;; C API scheme bindings
(define %libdispatch_so
  (dynamic-link
   (string-append (getenv"LITDOC_LIBDIR") "/libdispatch.so")))
(define (dispatch_init_module verbosity)
  (let* ((f (pointer->procedure
             void
             (dynamic-func "init_module" %libdispatch_so)
             (list int))))
    (f verbosity)))
(define (log_info . values)
  (let* ((line (apply string-append values))
         (ptr_line (string->pointer line))
         (proc (pointer->procedure
                void
                (dynamic-func "log_info" %libdispatch_so)
                (list '*))))
    (proc ptr_line)))
(define (log_warn  value)
  (let ((ptr_value (string->pointer value))
        (proc (pointer->procedure
               void
               (dynamic-func "log_warn" %libdispatch_so)
               (list '*))))
    (proc ptr_value)))
(define (log_error value)
  (let ((ptr_value (string->pointer value))
        (proc (pointer->procedure
               void
               (dynamic-func "log_error" %libdispatch_so)
               (list '*))))
    (proc ptr_value)))

(define (dispatch_version)
  (let ((proc (pointer->procedure
               int
               (dynamic-func "dispatch_version" %libdispatch_so)
               (list))))
    (proc)))
(define (dispatch_find document)
  (let ((ptr_document (string->pointer document))
        (proc (pointer->procedure
               int
               (dynamic-func "dispatch_find" %libdispatch_so)
               (list '*))))
    (proc ptr_document)))
(define (dispatch_lsdeps document)
  (let ((ptr_document (string->pointer document))
        (proc (pointer->procedure
               int
               (dynamic-func "dispatch_lsdeps" %libdispatch_so)
               (list '*))))
    (proc ptr_document)))

(define (dispatch_build document)
  (let* ((ptr_document (string->pointer document))
         (proc          (pointer->procedure
                         int
                         (dynamic-func "dispatch_build" %libdispatch_so)
                         (list '*))))
    (proc ptr_document)))
(define (dispatch_profile_options document runtime)
  (let ((ptr_document (string->pointer document))
        (ptr_runtime  (string->pointer runtime))
        (proc         (pointer->procedure
                       int
                       (dynamic-func
                        "dispatch_profile_options"
                        %libdispatch_so)
                       (list '* '*))))
    (stdout->string
     (lambda () (proc ptr_document ptr_runtime)))))
(define (dispatch_runtime_options document runtime)
  (let ((ptr_document (string->pointer document))
        (ptr_runtime  (string->pointer
                       (if  (string? runtime)
                            runtime "")))
        (proc         (pointer->procedure
                       int
                       (dynamic-func
                        "dispatch_runtime_options"
                        %libdispatch_so)
                       (list '* '*))))
    (stdout->string
     (lambda () (proc ptr_document ptr_runtime)))))

;;; Guix extension commands
(define (dispatch-init-module opts)
  (let* ((verbosity     (assoc-ref opts 'verbosity)))
    (dispatch_init_module
     (if (and (string? verbosity)
              (string->number verbosity))
         (string->number verbosity)
         1))))
(define (dispatch-version)
  (dispatch_version))
(define (dispatch-find document)
  (dispatch_find document))
(define (dispatch-lsdeps document)
  (dispatch_lsdeps document))
(define (dispatch-build document opts)
  (let* ((build-status  (dispatch_build document)))
    (when (= build-status 0)
      (let* ((cache-dir     (getenv "LITDOC_CACHEDIR"))
             (profile-dir   (string-append
                             cache-dir "/"
                             document
                             "/guix/"
                             document ))
             (runtime       (let ((r (assoc-ref opts 'runtime)))
                              (if (string? r) r "")))
             (profile-opts  (dispatch_profile_options
                             document runtime)))
        (mkdir-p profile-dir)
        (log_info "profile: " profile-opts)
        (apply run-guix-command
               'package
               (string-tokenize profile-opts))))))
(define (dispatch-run   document runtime)
  (let* ((runtime-options (dispatch_runtime_options
                           document runtime)))
    (log_info "run options: " runtime-options)
    (newline)
    (apply run-guix-command
           'shell
           (string-tokenize runtime-options))


    ))
