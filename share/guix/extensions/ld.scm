;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021-2023 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(set! %load-path
      (cons (string-append (getenv "LITDOC_DIR") "/share/")
            %load-path))

(define-module (guix extensions ld)
  #:use-module (guix extensions dispatch)
  #:use-module (guix ui)
  #:use-module ((guix diagnostics) #:select (location))
  #:use-module (guix scripts package)
  #:use-module (guix scripts shell)
  #:autoload   (guix scripts build) (show-build-options-help)
  #:autoload   (guix transformations) (show-transformation-options-help)
  #:use-module (guix scripts)
  #:use-module (guix packages)
  #:use-module (guix profiles)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-37)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:autoload   (ice-9 rdelim) (read-line)
  #:autoload   (guix base32) (bytevector->base32-string)
  #:autoload   (guix utils) (config-directory cache-directory)
  #:autoload   (guix describe) (current-channels)
  #:autoload   (guix channels) (channel-commit)
  #:autoload   (gcrypt hash) (sha256)
  #:use-module ((guix build utils) #:select (mkdir-p))
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (guix cache)
  #:use-module (system base compile)
  #:use-module ((ice-9 ftw) #:select (scandir))
  #:autoload   (ice-9 pretty-print) (pretty-print)
  #:autoload   (gnu packages) (cache-is-authoritative?
                               package-unique-version-prefix
                               specification->package
                               specification->package+output
                               specifications->manifest)
  #:export (guix-ld))


(dynamic-link "/home/arongile/.cache/litdoc/dispatch/lib/libdispatch.so")


;;
;; Run command
(define (litdoc-help)
  (newline)
  (display (G_ "Usage: guix ld [OPTION] COMAND DOCUMENT [-- SHELL-COMMAND...]
Build an environment configured using literate document DOCUMENT.\n"))
  (newline)
  (display "[OPTIONS]\n")
  (display (G_ "
   --config=CONFIG      select the configuration option to use\n"))
  (display (G_ "
  -h, --help            display this help and exit\n"))
  (display (G_ "
  -V, --version          display version information and exit\n"))

  (newline)
  (display "[COMMAND]\n")
  (display (G_ "
  find  DOCUMENT        find document DOCUMENT by filename\n"))

  (display (G_ "
  build  DOCUMENT       build the site for document DOCUMENT\n"))

  (display (G_ "
  inspect DOCUMENT      inspect document DOCUMENT\n"))

  (display (G_ "
  *run DOCUMENT         execute the literate document DOCUMENT\n"))
  (newline)
  (display "[-- DOC-COMMAND ...]\n")
  (display (G_ "
   -- DOC-COMMAND        one or more command arguments to be passed to the shell\n"))
  (show-bug-report-information))
(define (litdoc-unsupported args)
  (format #t  "[ERROR]: invalid command: ~a\n" args)
  (litdoc-help))


;;
;; cli argument processing
;;
(define (proc-arguments args)
  (reverse! (args-fold
             args
             (list (option '(#\h "help") #f #f
                           (lambda (opt name arg result)
                             (leave-on-EPIPE (litdoc-help))
                             (exit 0)))
                   (option '(#\V "version") #f #f
                           (lambda (opt name arg result)
                             (leave-on-EPIPE (dispatch-version))
                             (exit 0)))
                   (option '(#\r "runtime") #t #f
                           (lambda (opt name arg result)
                             (alist-cons 'runtime arg  result)))
                   (option '(#\v "verbosity") #t #f
                           (lambda (opt name arg result)
                             (alist-cons 'verbosity arg  result))))
             (lambda  _            (display "unknown option"))
             (lambda  (arg result) (cons arg result))
             (list ))))
(define (query-optional  parsed-args)

  (define (proc-options-recursive args result)
    (cond ((= 0 (length args))
           result)

          ((pair? (car args))
           (set! result  (cons (car args) result))
           (proc-options-recursive (cdr args) result))

          (#t
           (proc-options-recursive (cdr args) result))))


  (let ((result (list )))
    (proc-options-recursive parsed-args result)))
(define (query-positional  parsed-args)
  (define (proc-pos-recursive args result)
    (cond ((= 0 (length args))
           (reverse  result))

          ((not (pair? (car args)))
           (set! result (cons  (car args) result))
           (proc-pos-recursive (cdr args) result))

          (#t
           (proc-pos-recursive (cdr args) result))))

  (let (
        (result (list ))
        )
    (proc-pos-recursive parsed-args result)))


;;
;; main
;;
(define-command (guix-ld . args)
  (category development)
  (synopsis "spawn one-off software environments")
  (let* ((parsed-args (proc-arguments   args))
         (opts        (query-optional   parsed-args))
         (pos         (query-positional parsed-args)))
    (dispatch-init-module opts)
    (match pos
      (("find"     document)  (dispatch-find   document))
      (("lsdeps"   document)  (dispatch-lsdeps document))
      (("build"    document)  (dispatch-build  document opts))
      (("run"      document)  (dispatch-run    document opts))
      (("help")               (litdoc-help))
      ((document)             (dispatch-run document opts))
      (_                      (litdoc-unsupported args)))))
