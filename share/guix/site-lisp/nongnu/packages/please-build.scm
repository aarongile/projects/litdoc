;; Copyright (C) 2024 <Aaron Gile(aronggile@gmail.com)>

;; This script is part of LitDoc.

;; LitDoc is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; LitDoc is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with LitDoc.  If not, see <https://www.gnu.org/licenses/>.



(define-module (nongnu packages please-build)
  #:use-module (guix)
  #:use-module (guix packages)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (ice-9 format)
  #:use-module ((guix licenses) #:prefix license:))

(define please-build-repo-url-format
  "https://github.com/thought-machine/please/releases/download/v~a/please_~a_~a")

(define-public please-build
  (package (name "please-build")
           (version "17.8.3")
           (build-system  copy-build-system)
           (source (origin (method url-fetch)
                           (uri (format #f please-build-repo-url-format version version "linux_amd64"))
                           (sha256 (base32 "1q9dj9cjn98hhnq9gialnd8air66d6zwgwzmkxriwqr2laq847a5"))))
           (arguments
            (let*
                ((src_file       (format #f "please_~a_~a" version "linux_amd64"))
                 (out_file      `(lambda _ (chmod ,src_file #o777))))
              `(#:install-plan
                '((,src_file "bin/plz"))
                #:phases
                (modify-phases %standard-phases (add-after 'unpack 'out_file ,out_file)))))
           (home-page "https://please.build")
           (synopsis "Please is a cross-language build system." )
           (description
            (string-append
             "Please is a cross-language build system with an emphasis on high performance,"
             "extensibility and reproducibility. It supports a number of popular languages "
             "and can automate nearly any aspect of your build process."))
           (license  license:asl2.0)))
