#! /usr/bin/env sh
### SPDX-License-Identifier: GPL-3.0-or-later
### Copyright © 2024 Aron Gile <aronggile@gmail.com>

set -e
set -o pipefail

export SSL_CERT_DIR="/etc/ssl/certs"
export SSL_CERT_FILE="/etc/ssl/certs/ca-certificates.crt"
export GIT_SSL_CAINFO="$SSL_CERT_FILE"
export GUIX_LOCPATH="/usr/lib/locale"

source "${LITDOC_DIR}/share/sh/log.sh"

[ $# -ge 1 ] || {
    log_error "usage: ldconf build|develop [OPTIONS]"
    exit 1
}

cmd="${1}"; shift
case "${cmd}" in
    build)
        log_subinfo "shell(container): /opt/litdoc/src"
        cd "${LITDOC_SRCDIR}" || {
            log_error "[litdoc(build)]: unable to locate src dir"
            log_error "[litdoc(build)]: is LITDOC_SRCDIR defined?"
            exit 1
        }

        [ -d "${LITDOC_BINDIR}" ] || {
            	log_error "unable to read the build output directory: <$LITDOC_BINDIR>"
            	log_error "is LITDOC_BINDIR defined?"
        }
        [ -d "${LITDOC_LIBDIR}" ] || {
            log_error "unable to read the build output lib directory: <$LITDOC_LIBDIR>"
            log_error "is LITDOC_LIBDIR defined?"
            exit 1
        }
        plz build --noupdate --verbosity=3 --config=opt '//src/dispatch:libdispatch' || exit 1
        plz query output '//src/dispatch:libdispatch' | xargs -I {} mv {} "${LITDOC_LIBDIR}/libdispatch.so"
        exit 0
        ;;
    develop)
        log_info "litdoc development environment"
        log_info "to build litdoc run:"
        log_info "cd ${LITDOC_SRCDIR} && plz build"
        exec bash
        ;;
    *)
        log_error "unknown command: $cmd"
        exit 1
        ;;
esac
