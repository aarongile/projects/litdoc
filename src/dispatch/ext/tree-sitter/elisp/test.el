(use-package org
  :guix (:name emacs-orgy :profile default)
  <<org-config>>
  :mode
  ("\\.org" . org-mode)
  :hook
  (org-mode . org-indent-mode)
  :init
  ;; org init
  << org-init >>
  ;; keybindings
  :general
  << org-kb >>
  :custom
  (enable-local-variables :all)
  (enable-local-eval t)
  ;; never leave empty lines in collapsed view
  (org-cycle-separator-lines 0)
  ;;config
  :config
  << org-config >>)

(use-package xournal-app
  :guix (:name emacs-xournal-app :profile dev)
  :mode
  ("\\.org" . org-mode)
  :hook
  (org-mode . org-indent-mode)
  :init
  ;; org init
  << org-init >>
  ;; keybindings
  :general
  << org-kb >>
  :custom
  (enable-local-variables :all)
  (enable-local-eval t)
  ;; never leave empty lines in collapsed view
  (org-cycle-separator-lines 0)
  ;;config
  :config
  << org-config >>)
