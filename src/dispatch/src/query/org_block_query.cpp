// SPDX-License-Identifier: GPL-3.0-or-laterr
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "query/org_block_query.hpp"
#include "query/org_block.hpp"
#include "config/config_settings.hpp"
#include "org/org_file.hpp"

#include <tree_sitter/api.h>
#include <fmt/core.h>

#include <vector>
#include <span>
#include <stack>
#include <cassert>
#include <algorithm>
#include <iostream>

extern "C" TSLanguage* tree_sitter_org();
namespace litdoc {

  static const std::string DEFAULT_QUERY_PATTERN {
    "(block name: (expr)@name  parameter: (expr)+ @param (contents)? @contents)"
  };

  auto to_string_view(
   std::string_view content,
   std::uint32_t start_byte,
   std::uint32_t end_byte)
    -> std::string_view {
    if( start_byte == end_byte ) {
      return std::string_view("");
    }
    const char* data = content.data();
    const char* first = data + start_byte;
    const char* last  = data + end_byte;
    return std::string_view(first,last);
  }
  auto to_block(const TSQueryMatch& match, std::string_view source ) -> org_block {

    enum BLOCK_FIELD : std::uint32_t {
      BLOCK_FIELD_NAME     = 0,
      BLOCK_FIELD_PARAM    = 1,
      BLOCK_FIELD_CONTENTS = 2
    };

    org_block b;
    std::vector<std::string_view> raw_params;
    std::span<const TSQueryCapture> captures{ match.captures, match.capture_count };
    for( const auto& capture : captures) {
      auto start_byte     = ts_node_start_byte(capture.node);
      auto end_byte       = ts_node_end_byte(capture.node);
      switch( capture.index ) {

      case BLOCK_FIELD_NAME: {
        b.type = to_string_view(source,start_byte,end_byte);
      } break;

      case BLOCK_FIELD_PARAM: {
        auto param = to_string_view(source,start_byte,end_byte);
        raw_params.push_back(param);

      } break;

      case BLOCK_FIELD_CONTENTS: {
        b.content = to_string_view(source,start_byte,end_byte);
      } break;

      };
    }

    b.language = raw_params[0];
    std::stack<std::string_view> keys;
    for( auto const& param : raw_params)
      {
        if(param.starts_with(":"))
        {
          auto key = std::string_view(param.begin() + 1, param.end());
          keys.push(key);
        }
        else
          {
            if( !keys.empty() )
              {
                auto key = keys.top(); keys.pop();
                b.parameters[key] = param;
              }
            else
              {
                b.parameters[param] = "";
              }
          }
      }

    return b;
  }
  auto to_error_text( TSQueryError code ) -> std::string_view {

    switch(code) {
    case TSQueryErrorSyntax:
      return std::string_view("syntax error");
      break;
    case TSQueryErrorNodeType:
      return std::string_view("invalid node type");
      break;
    case TSQueryErrorField:
      return std::string_view("invalid field name or value");
      break;
    case TSQueryErrorCapture:
      return std::string_view("invalid capture string");
      break;
    case TSQueryErrorStructure:
      return std::string_view("invalid query structure");
      break;
    case TSQueryErrorLanguage:
      return std::string_view("invalid org-mode document(unsupported format)");
      break;
    default:
      return std::string_view("ok");
      break;
    };
  }
  ////////////////////////////////////////////////////////////////////
  //org_block_query
  ////////////////////////////////////////////////////////////////////

  struct org_block_query::impl {
    std::shared_ptr<TSParser>      parser;
    std::shared_ptr<TSQuery>       query;
    std::shared_ptr<TSQueryCursor> cursor;
  };

  org_block_query::org_block_query( )
    :pimpl{ new impl } {

    //langaue
    auto ptr_lang = tree_sitter_org();

    //parser
    pimpl->parser = std::shared_ptr<TSParser>(ts_parser_new(), ts_parser_delete);
    if(!ts_parser_set_language( pimpl->parser.get(),ptr_lang) ) {
      throw std::runtime_error{ "unable to set org-mode document parser language" };
    }

    //query
    std::uint32_t error_offset  = 0;
    TSQueryError error_type     = TSQueryErrorNone;
    this->pimpl->query = std::shared_ptr<TSQuery>(
     ts_query_new(
      ptr_lang,
      DEFAULT_QUERY_PATTERN.c_str(),
      DEFAULT_QUERY_PATTERN.size(),
      &error_offset,
      &error_type),
     ts_query_delete
    );
    if( error_type != TSQueryErrorNone ) {
      const std::string what{ to_error_text(error_type) };
      throw std::runtime_error{ what };
    }

    this->pimpl->cursor = std::shared_ptr<TSQueryCursor>{
      ts_query_cursor_new(),
      ts_query_cursor_delete
    };

  }

  auto org_block_query::operator()(const org_file& source ) -> org_block_query::result_type {

    auto content     = source.content();
    auto syntax_tree = std::shared_ptr<TSTree>(
     ts_parser_parse_string( this->pimpl->parser.get(), 0, content.data(), content.size()),
     ts_tree_delete
    );

    if(!syntax_tree) {
      throw std::runtime_error{
        fmt::format("failed to parse org file: {}", source.path())
      };
    }

    auto root_node  = ts_tree_root_node(syntax_tree.get());
    ts_query_cursor_exec(
     pimpl->cursor.get(),
     pimpl->query.get(),
     root_node);

    org_block_query::result_type blocks;
    TSQueryMatch match;
    auto pcursor = this->pimpl->cursor.get();
    while(ts_query_cursor_next_match(pcursor,&match)) {

      auto b = to_block(match,content);
      if( b.type == "src" ) {
        blocks.push_back(b);
      }

    }
    return blocks;
  }
}
