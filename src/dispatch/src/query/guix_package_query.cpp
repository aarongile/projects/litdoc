// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "query/guix_package_query.hpp"
#include <spdlog/spdlog.h>
#include <tree_sitter/api.h>
#include <fmt/core.h>
#include <memory>
#include <span>
#include <vector>
#include <exception>

extern "C" TSLanguage* tree_sitter_elisp( );

namespace litdoc {

		static const std::string PACKAGE_QUERY {
          "(usepackage_def " \
          "(package_spec (package_name name:(symbol)@name)" \
          "(version version:(symbol)@version)? ))"
		};


  auto to_package_error_str( TSQueryError code ) -> std::string_view {

				switch(code) {
				case TSQueryErrorSyntax:
						return std::string_view("syntax error");
						break;
				case TSQueryErrorNodeType:
						return std::string_view("invalid node type");
						break;
				case TSQueryErrorField:
						return std::string_view("invalid field name or value");
						break;
				case TSQueryErrorCapture:
						return std::string_view("invalid capture string");
						break;
				case TSQueryErrorStructure:
						return std::string_view("invalid query structure");
						break;
				case TSQueryErrorLanguage:
						return std::string_view("parser error");
						break;
				default:
						return std::string_view("ok");
						break;
				};
		}

  auto to_package_str(
      std::string_view content,
      std::uint32_t start_byte,
      std::uint32_t end_byte)
      -> std::string_view {

      if( start_byte == end_byte ) {
          return std::string_view("");
      }
      const char* data = content.data();
      const char* first = data + start_byte;
      const char* last  = data + end_byte;
      return std::string_view(first,last);
  }


  auto to_guix_package(const TSQueryMatch& match, std::string_view source) -> guix_package {

    enum PACKAGE_FIELDS : std::uint32_t {
      PACKAGE_FIELDS_NAME     = 0,
      PACKAGE_FIELDS_VERSION  = 1,
    };

    guix_package package;
    std::span<const TSQueryCapture> captures{ match.captures, match.capture_count };
    for( const auto& capture : captures) {
        auto start_byte     = ts_node_start_byte(capture.node);
        auto end_byte       = ts_node_end_byte(capture.node);
        switch( capture.index ) {

        case PACKAGE_FIELDS_NAME: {
            package.name = to_package_str(source,start_byte,end_byte);
            spdlog::info("- package: {}", package.name);
        } break;
        case PACKAGE_FIELDS_VERSION: {
            package.version = to_package_str(source,start_byte,end_byte);
            spdlog::info("- package version: {}", package.version);
        } break;
        default: {
          auto unknown = to_package_str(source, start_byte, end_byte);
          spdlog::warn("- parsed failed to process unknown property: {}", unknown);
        }
      };
    }
    return package;
  }

  struct guix_package_query::impl {
    std::shared_ptr<TSParser> parser;
				std::shared_ptr<TSQuery>  query;
				std::shared_ptr<TSQueryCursor> cursor;
		};


  guix_package_query::guix_package_query( )
    :pimpl( new impl ) {

				//langaue
				auto ptr_lang = tree_sitter_elisp();

				//parser
				pimpl->parser = std::shared_ptr<TSParser>(ts_parser_new(), ts_parser_delete);
				if(!ts_parser_set_language( pimpl->parser.get(),ptr_lang)) {

                  throw std::runtime_error{
                    "unable to load set specialized elisp parser"
                  };

				}

				//Guix package query
				{
						std::uint32_t error_offset = 0;
						TSQueryError  error_type   = TSQueryErrorNone;
						auto pquery = ts_query_new(
								ptr_lang,
								PACKAGE_QUERY.c_str(),
								PACKAGE_QUERY.size(),
								&error_offset,
								&error_type);
						this->pimpl->query = std::shared_ptr<TSQuery>(pquery,ts_query_delete);
						if( error_type != TSQueryErrorNone ) {
								const std::string what{ to_package_error_str(error_type) };
                                  throw std::runtime_error(what);
                        }
				}

				this->pimpl->cursor = std::shared_ptr<TSQueryCursor>{
						ts_query_cursor_new(),
						ts_query_cursor_delete
				};
		}


  bool
  guix_package_query::operator()(std::string_view content, std::vector<guix_package>& packages ) const
  {

          auto ptree = std::shared_ptr<TSTree>{
              ts_parser_parse_string(
                  this->pimpl->parser.get(),
                  0,
                  content.data(),
                  content.size()),
              ts_tree_delete
          };

          if(!ptree) {
              throw std::runtime_error{"failed to parse elisp content"};
          }

          auto root_node  = ts_tree_root_node(ptree.get());
          ts_query_cursor_exec(
              pimpl->cursor.get(),
				pimpl->query.get(),
              root_node);
          TSQueryMatch match;
          auto pcursor = this->pimpl->cursor.get();
          while(ts_query_cursor_next_match(pcursor,&match)) {
              auto package = to_guix_package(match,content);
              packages.push_back(package);
          }

          return true;
  }


}//end namespace
