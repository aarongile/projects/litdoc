// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "config/config_settings.hpp"
#include <string>
#include <string_view>
#include <cstring>
#include <cstdlib>
#include <cctype>
#include <stdexcept>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <utility>

namespace litdoc {


  auto get_path(std::string_view str) -> std::pair<std::string_view, std::string_view>
  {
    auto left = str.begin();
    for (auto it = left; it != str.end(); ++it) {
      if (*it == ':')
        {
          return {
            {&*left, size_t(it - left)},
            {&*(it + 1), size_t(str.end() - it - 1)}
          };
        }

    }

    return {
      {*&left, size_t(str.end() - left)},
      {""}
    };

  }

  inline const char* begin( const char* ptr ) {
    return ptr;
  }
  inline const char* end( const char* ptr ) {
    return ptr + std::strlen(ptr);
  }

  config_settings config_settings::get_current( )
  {

    config_settings curr;



    //cache directory
    {
      const char* ptr_cachedir = std::getenv("LITDOC_CACHEDIR");
      if(ptr_cachedir == nullptr)
        curr.cache_directory = std::string("~/.cache/litdoc");
      else
        curr.cache_directory = std::string(ptr_cachedir);
    }

    //root directory
    {
      const char* root_dir = std::getenv("LITDOC_DIR");
      if(root_dir == nullptr )
        throw std::logic_error("missing a required environment variable: LITDOC_DIR");
      curr.root_directory = std::string(root_dir);
    }

    //search paths
    {
      const char* litdoc_path = std::getenv("LITDOC_PATH");
      if( litdoc_path != nullptr) {

        const std::string_view str(litdoc_path);
        auto &paths = curr.search_paths;
        auto p = get_path(str);
        while( p.first != "" ) {
          const std::string p_str(p.first);
          paths.push_back(p_str);
          p = get_path(p.second);
        };
      }
    }

    return curr;
  }

  std::string config_settings::version() const {
    const char* version = std::getenv("LITDOC_VERSION");
    if( version == nullptr )
      throw std::logic_error("missing a required environmental variable: LITDOC_VERSION");
    //TODO: validate version format
    return std::string(version);
  }
  bool config_settings::enable_verbose() const {

    const char* verbosity = std::getenv("LITDOC_VERBOSE");
    if( verbosity == nullptr ) {
      return false;
    }

    //read from environment
    bool is_verbose;
    {
      const auto to_lower = []( char value ) -> char
      {
        return static_cast<char>( std::tolower( static_cast<int>(value) ) );
      };
      std::string str{verbosity};
      std::transform(str.begin(), str.end(), str.begin(), to_lower);
      std::istringstream is(str);
      is >> std::boolalpha >> is_verbose;
    }

    return is_verbose;
  }

}
