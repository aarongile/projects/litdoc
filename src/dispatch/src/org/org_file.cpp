// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include <spdlog/spdlog.h>
#include <fmt/core.h>

#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <ranges>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <queue>
#include <filesystem>
#include <algorithm>

#include "fs/trim.hpp"
#include "config/config_settings.hpp"
#include "org/org_file.hpp"

namespace fs = std::filesystem;
namespace rv = std::ranges::views;
namespace litdoc {

  //////////////////////////////////////////////////////////
  // dependecy graph
  //////////////////////////////////////////////////////////
  struct dep_node {
    std::string name;
    int indegrees;
    std::vector<std::string> deps;
  };

  struct org_depgraph
  {

    inline explicit org_depgraph( const org_file& root )
      :root{root},
       config{ config_settings::get_current() },
       nodes{ },
       errors{} {
    }

    bool getdeps(std::vector<std::string>& deps );

  private:

    void init_queue( std::queue<dep_node>& value );
    bool build_deps( const std::string& doc);

    const org_file& root;
    config_settings config;
    std::unordered_map<std::string ,dep_node> nodes;
    std::vector<std::string> errors;

  };

  void org_depgraph::init_queue( std::queue<dep_node>& qeue ) {
    for( const auto& [name,node] : nodes ) {
      if(node.indegrees == 0) {
        qeue.push(node);
      }
    }
  }

  bool org_depgraph::build_deps( const std::string& name) {

    //already added?
    {
      auto pos = nodes.find(name);
      if( pos != nodes.end() ){
        return true;
      }
    }

    //get document
    std::string_view name_view{ name.begin(), name.end() };
    auto doc_path = org_find(name_view,this->config);
    if(!doc_path) {
      errors.push_back(name);
      return false;
    }

    //init node
    dep_node node { .name = name , .indegrees = 0, .deps = { } };
    {
      org_file doc{*doc_path};
      doc.getdeps(node.deps);
      for( const auto& dep : node.deps ) {
        auto pos = nodes.find(dep);
        if( pos != nodes.end() )
          pos->second.indegrees++;
      }
      nodes.insert( std::make_pair(name,node) );
    }

    // build child dependecies
    for( const auto& dep : node.deps ){
      auto pos = nodes.find(dep);
      if( pos == nodes.end() ) {
        if(!build_deps(dep))
          errors.push_back(dep);
      }
    }

    return true;
  }


  bool org_depgraph::getdeps( std::vector<std::string>& deps ) {

    this->nodes.clear();
    this->errors.clear();

    std::vector<std::string> root_deps;
    root.getdeps(root_deps);
    for(const auto& root_dep : root_deps )
      build_deps(root_dep);

    std::queue<dep_node> q;
    init_queue(q);
    while( q.size() > 0 ) {
      auto& curr = q.front();
      deps.push_back(curr.name);
      q.pop();
      for( auto& [name,node] : this->nodes ) {
        auto first = node.deps.begin();
        auto last  = node.deps.end();
        auto pos = std::find(first, last,name);
        if( pos != last) {
          node.deps.erase(pos);
          node.indegrees--;
          if(node.indegrees == 0)
            q.push(node);
        }
      }
    }
    if(deps.size() != nodes.size()){
      //cyclic dependecy error
      return false;
    }
    return true;
  }



  ///////////////////////////////////////////////////////
  // org_file::impl
  ///////////////////////////////////////////////////////
  struct org_file::impl {

    explicit impl( const std::string& file_path )
      :path(file_path),
       name( fs::path(file_path).stem().generic_string()){
      //open file for read operations
      auto fd = open(file_path.c_str(), O_RDONLY);
      if( fd < 0) {
        throw std::runtime_error( fmt::format("unable to open file: {}", file_path) );
      }

         this->descriptor = fd;

         //file stasus
         struct stat file_status;
         if (fstat(fd, &file_status) < 0 || file_status.st_size <= 0 ) {
           throw std::runtime_error(fmt::format("unable to read file status(fstat): {}", file_path ));
         }

         //TODO: load bigger org files in chunks?
         auto raw_data = mmap(0, file_status.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
         if( raw_data == MAP_FAILED ) {
           throw  std::runtime_error(fmt::format("unable to map file into memory: {}", file_path));
}
      this->data = reinterpret_cast<char*>(raw_data);
      this->size = file_status.st_size;
    }
    ~impl() {

      if(this->data != 0 ) {
        munmap(this->data, this->size);
        this->size = 0;
        this->data = 0;
      }

      if( this->descriptor != 0 )  {
        close(this->descriptor);
        this->descriptor = 0;
      }
    }
    char*                data = 0;
    size_t               size = 0;
    int                  descriptor = -1;
    std::string          path;
    std::string          name;
  };



  ///////////////////////////////////////////////////////
  // org_file
  ///////////////////////////////////////////////////////
  org_file::org_file( const std::string& file_path )
        :pimpl{ new impl(file_path) } {

      }

  auto org_file::content() const -> std::string_view {
    auto first = pimpl->data;
    auto last  = pimpl->data + pimpl->size;
    return std::string_view(first,last);
  }


  auto org_file::path() const -> const std::string& {
    return pimpl->path;
  }

  auto org_file::name() const -> const std::string& {
    return pimpl->name;
  }
  ////////////////////////////////////////////////////////////////////////////////
  //orgfile::getdeps
  ////////////////////////////////////////////////////////////////////////////////
  static const std::string_view DEPDECL_KEYWORD = "#+workflows:";
  void to_vector( std::string_view dep_expr, std::vector<std::string>& deps ) {
    std::string dep_str{dep_expr};
    std::istringstream stream{dep_str};
    std::string dep;
    while(std::getline(stream,dep,' ')) {
      if(!dep.empty())
        deps.push_back(dep);
    }

  }
  inline auto
  skip_usedoc_keyword( std::string_view line ) -> std::string_view {
    return line.substr(DEPDECL_KEYWORD.size());
  }

  auto is_org_header( std::string_view line ) -> bool {
    line = trim(line);
    return line.starts_with("*");
  }

  auto org_file::getdeps( std::vector<std::string>& deps ) const -> void {
    const auto delm = std::string_view{"\n"};
    auto rngs = this->content() | rv::split(delm);
    for(auto &&rng : rngs ) {
      auto line = std::string_view(&*rng.begin(), std::ranges::distance(rng));
      line = trim(line);
      if( line.starts_with(DEPDECL_KEYWORD)) {
        auto str_deps = skip_usedoc_keyword(line);
        to_vector(str_deps,deps);
        break;
      }
      if( is_org_header(line) )
        break;
    }
  }

  auto org_file::getdeps_full( std::vector<std::string>& deps ) const -> void {
    org_depgraph g(*this);
    if( !g.getdeps(deps) ) {
      spdlog::warn( "failed to resolve one or more docs" );
    }
  }
  ///////////////////////////////////////////////////////////////////
  // org_find
  ///////////////////////////////////////////////////////////////////
  auto org_find(const std::string_view org_name,
                const config_settings& config)->std::optional<std::string> {

    spdlog::info("searching: <{}> ...", org_name);
    //org file in the current working directory?
    {
      auto current_path{fs::current_path()};
      std::string_view cwd( current_path.c_str() );
      fs::path p{cwd};
      p /= fmt::format("{}.org", org_name);
      spdlog::info("- in working directory: <{}> ...",  p.c_str());
      if( fs::exists(p) ) {
        spdlog::info("=> [FOUND] ... {}", p.c_str());
        return std::optional<std::string>(p.generic_string());
      }

    }

    // //org file in LITDOC_DIR?
    {
      auto p = fs::path(fmt::format("{}/commands/{}/{}.org",
                                    config.root_directory,
                                    org_name, org_name));
      spdlog::info("- in default directory: <{}> ...", p.c_str());
      if(fs::exists(p)) {
        spdlog::info("=> [FOUND] ... {}", p.c_str());
        org_file file( p.c_str());
        return std::optional<std::string>{p.generic_string()};
      }
    }

    //org file in search path?
    {
      for( const auto& search_path : config.search_paths ) {
          fs::recursive_directory_iterator it(search_path);
          spdlog::info("- in user search directory: <{}> ...", search_path.c_str());
          for( auto &entry : it ) {
            auto p = entry.path();
            if(
               fs::is_regular_file(p)
               && p.extension() == ".org"
               && p.stem() == org_name
               ) {
              spdlog::info("=> [FOUND] <{}>: {}", org_name,p.c_str());
              return std::optional<std::string>{p.generic_string()};
            }

          }
      }
    }


    //not found
    spdlog::warn("=> [FAILED] <{}>: org file not found", org_name);
    return std::optional<std::string>{};
  }



} //namespace
