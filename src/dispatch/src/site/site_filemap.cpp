// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "site/site_filemap.hpp"
#include "site/site_file.hpp"
#include "site/org_site.hpp"
#include "site/site_submap.hpp"
#include "org/org_file.hpp"
#include "fs/ensure_path.hpp"

#include <iostream>
#include <algorithm>
#include <filesystem>
#include <ranges>
#include <vector>

#include <spdlog/spdlog.h>
#include <fmt/core.h>

namespace fs = std::filesystem;
namespace rv = std::ranges::views;
namespace rn = std::ranges;
namespace litdoc {


  auto add_symlink( const org_site& site, std::string_view filename )-> bool
  {

    const auto source_dir  =  fs::path{site.source().path()}.parent_path();
    const auto site_dir    =  fs::path{site.directory()};
    const auto file_path   =  fs::path{site.source().path()};
    const auto file_link   =  site_dir / filename;
    if( !fs::exists(file_link) && fs::exists(file_path) ) {
      std::error_code err;
      fs::path site_path{site_dir};
      if( !ensure_path(site_path,err)) {
        spdlog::error("- failed to create directory: {}", site_path.c_str());
        return false;
      }

      err.clear();
      fs::create_symlink(file_path,file_link,err);
      if(err) {
        spdlog::error("- failed to create symlink <{}> -> <{}> with error: {}",
                      file_link.c_str(),
                      file_path.c_str(),
                      err.message());
        return false;
      }
    }

    spdlog::info("- added symlink: <{}> -> <{}>",
                 file_link.c_str(),
                 file_path.c_str());
    return true;

  }



  void site_filemap::_rmv_sitedir() {

    auto site_dir = fs::path{ this->_site.directory() };
    std::error_code err;
    auto count = fs::remove_all(site_dir,err);
    if(err) {
      spdlog::warn("- failed to remove site directory: <{}>", site_dir.c_str());
    } else {
      spdlog::info("- deleted {} files in site directory:", count, site_dir.c_str());
    }
  }

  void site_filemap::_add_blocks(const std::vector<org_block>& blocks)
  {

    const auto is_src_block = [ ]( const org_block& block ) {
      return block.is_enabled()
        && block.is_src()
        && !block.is_sub();
    };

    const org_site& site = _site;
    std::unordered_map<std::string,site_file>& map = _map;
    rn::for_each(blocks | rv::filter(is_src_block), [ &site, &map ]( const org_block& block )
    {
      std::string tangle     = block.get_param("tangle",site.name());
      std::string language   = std::string{ block.language };
      std::string file_id    =  make_fileid(tangle,language);
      auto pos = map.find(file_id);
      if( pos != map.end()) {
        site_file& file = pos->second;
        file.add_block(block);
      } else {
        site_file file(site,block);
        map.insert(std::make_pair(file_id,file));
      }});

  }


  auto site_filemap::build( const std::vector<org_block>& blocks ) -> bool
  {
    site_submap   submap{ this->_site  };
    submap.add_blocks(blocks);
    this->_add_blocks(blocks);

    spdlog::info("- {} entries are subistitution blocks", submap.size());
    spdlog::info("- {} entries are source blocks", this->_map.size());

    this->_rmv_sitedir();

    spdlog::info("- writing files to directory: {}", this->_site.directory());
    for( auto& [fileid,file] : _map ) {
      //write file
      if(file.commit(submap)) {
        spdlog::info("- write({}): {} .... [OK]", this->_site.name(), file.path());
      } else {
        spdlog::warn("- write({}): {} .... [FAILED]", this->_site.name(), file.path());
      }
    }

    //add symbolic link to source
    auto org_filename = fs::path{ this->_site.source().path() }.filename().generic_string();
    add_symlink(this->_site,org_filename);
    return true;
  }





}
