// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "site/org_site.hpp"
#include "org/org_file.hpp"
#include "config/config_settings.hpp"
#include "query/org_block_query.hpp"
#include "site/site_fingerprint.hpp"
#include "site/site_filemap.hpp"
#include "site/site_manifest.hpp"


#include <spdlog/spdlog.h>

#include <stdexcept>
#include <algorithm>
#include <ranges>
#include <unordered_map>
#include <filesystem>
#include <fstream>
#include <iostream>

namespace fs = std::filesystem;
namespace litdoc {


  ///////////////////////////////////////////////////////////////////
  // org_site
  ///////////////////////////////////////////////////////////////////
  org_site::org_site(const org_file& source, const config_settings& config)
    :_source{source},
     _config{config}
  {
    fs::path org_file{source.path()};
    this->_name = org_file.stem();
    this->_dir  = (fs::path{ config.cache_directory } / _name).generic_string();
  }

  auto org_site::build( ) -> build_status
  {
    spdlog::info("=> [{}]: building site ... ", this->_name);
    litdoc::site_fingerprint fingerprint(*this);
    if(fingerprint.check()) {
      spdlog::info("site fingerprint up-to date");
      spdlog::info("=> [{}]: building site ... [DONE]", this->_name);
      return build_status::SKIPPED;
    }

    //build site dependecies
    {
      std::vector<std::string> site_deps;
      this->source().getdeps_full(site_deps);
      for( const auto& site_dep : site_deps) {
        spdlog::info("- dep: {}",site_dep);
        const auto dep_file = org_find(site_dep, this->config());
        if(!dep_file) {
          spdlog::warn("- failed to read site<{}> dependency:{}",this->_name,site_dep);
          continue;
        }
        org_file dep_source{*dep_file};
        org_site dep_site{dep_source,this->config()};
        auto status = dep_site.build();
        if( status == build_status::ERROR ) {
          spdlog::warn("- failed to build site<{}> dependency:{}",this->_name,site_dep);
        }

      }
    }

      //tangle file to in-memory blocks
      org_block_query block_query;
      auto blocks = block_query( this->source());
      spdlog::info("- examining {} org blocks", blocks.size());
      litdoc::site_filemap filemap{ *this };
      if( !filemap.build(blocks) ) {
        spdlog::error("[{}]:failed to build site files", this->_name);
        return build_status::ERROR;
      }

      //write blocks to file(s)
      site_manifest manifest{ *this };
      if(!manifest.build(blocks)){
        spdlog::error("- {}:failed to build site manifest", this->_name);
        return build_status::ERROR;
      }

      //make fingerprint
      if(!fingerprint.build()) {
        spdlog::error("- {}: failed to build site fingerprint", this->_name);
        return build_status::ERROR;
      }

      spdlog::info("=> [{}]: building site ... [DONE]", this->_name);
      return build_status::OK;
  }


}

//namespace
