// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "site/site_manifest.hpp"

#include "config/config_settings.hpp"
#include "fs/ensure_path.hpp"
#include "site/org_site.hpp"
#include "query/guix_package_query.hpp"
#include <spdlog/spdlog.h>
#include <filesystem>
#include <fstream>
#include <ranges>
#include <vector>
#include <algorithm>

namespace fs = std::filesystem;
namespace rv = std::ranges::views;
namespace litdoc {

  auto
  get_manifest_path(const org_site& site, std::string& manifest_scm ) -> bool {
    std::error_code err;
    const auto manifest_dir = site.directory() + "/etc/";
    if(!ensure_path(manifest_dir,err)) {
      auto what =  fmt::format("- unable to create manifest directory: <{}> ... {}",
                               manifest_dir,
                               err.message());
      spdlog::critical("failed to create manifest directory: '{}', {}", manifest_dir, what);
      return false;
    }
    auto p = fs::path{manifest_dir} / "manifest.auto.scm";
    manifest_scm = p.generic_string();
    return true;
  }


  bool
  write_manifest(const org_site& site, const std::vector<guix_package>& packages ) {

    if( packages.size() == 0 )
      return true;

    std::string manifest_scm;
    if(!get_manifest_path(site,manifest_scm)) {
      return false;
    }
    spdlog::info("- manifest file: {}", manifest_scm);
    if(std::ofstream stream{manifest_scm, std::ios::out | std::ios::trunc }; stream) {
      stream << "(specifications->manifest '(" << std::endl;
      for( const auto& package : packages ) {
        spdlog::info("-add package: {}", package.name);
        stream << "\"" << package.name << "\"" << std::endl;
      }
      stream << "))" << std::endl << std::flush;
    } else {
      spdlog::error("unable to open for write: {}", manifest_scm );
    }
    return true;
  }

  /////////////////////////////////////////////////////////////////////
  // site_manifest
  /////////////////////////////////////////////////////////////////////
  site_manifest::site_manifest( const org_site& site )
    :_site(site) {
  }


  bool site_manifest::build( const std::vector<org_block>& blocks ) {

    const auto is_elisp_block = []( const org_block& block ) {
      return block.is_enabled()
        && block.is_src()
        && ( block.language == "emacs-lisp" || block.language == "elisp" );
    };

    guix_package_query query;
    std::vector<guix_package> packages;
    const auto add_package = [ &query, &packages ]( const org_block& elisp_block ) {
      query(elisp_block.content,packages);
    };
    std::ranges::for_each( blocks | rv::filter(is_elisp_block),add_package);

    return write_manifest( this->_site, packages);

  }



}
