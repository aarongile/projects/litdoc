// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "site/site_file.hpp"

#include "fs/ensure_path.hpp"
#include "fs/trim.hpp"
#include "site/site_submap.hpp"
#include "site/org_site.hpp"

#include <filesystem>
#include <ranges>
#include <wordexp.h>
#include <spdlog/spdlog.h>
#include <fmt/core.h>

namespace  rv = std::ranges::views;
namespace fs  = std::filesystem;

namespace litdoc {

  ///////////////////////////////////////////////////////////
  //utilities
  ////////////////////////////////////////////////////////////


  auto is_scheme(std::string_view language ) -> bool
  {
    return ( language == "scheme" || language == "guile" );
  }

  auto is_shell(std::string_view language ) -> bool
  {
    return ( language == "shell" ||
             language == "sh"    ||
             language == "bash" );
  }

  auto is_elisp(std::string_view language ) -> bool
  {
    return ( language == "emacs-lisp" || language == "elisp"  );
  }


  auto is_shell_option(const std::string&  language , const std::string& tangle_name ) -> bool
  {
    if(!is_shell(language))
      return false;

    if( tangle_name == "host.opt" || tangle_name == "container.opt" ) {
      return true;
    }

    if( tangle_name.ends_with(".opt") ) {
      return true;
    }

    return false;
  }

  auto is_shell_env(const std::string&  language , const std::string& tangle_name ) -> bool
  {
    if(!is_shell(language))
      return false;

    if( tangle_name == "host.env" || tangle_name == "container.env" ) {
      return true;
    }

    if( tangle_name.ends_with(".opt") ) {
      return true;
    }

    return false;
  }

  auto
  make_filepath(const std::string& site_name, const std::string& tangle_name,
                const std::string& root_dir, const std::string& language) -> std::string {
    fs::path file_path;
    fs::path root_dir_path{ root_dir };

    //main
    if( is_shell(language) && tangle_name == "main") {
      std::string p{"bin/"}; p +=site_name; p+= ".sh";
      return (root_dir_path / p).generic_string();
    }

    //manifest
    if( is_scheme(language) && ((tangle_name == "manifest") ||(tangle_name == "manifest.scm")) ) {
      return (root_dir_path / "etc/manifest.scm" ).generic_string();
    }

    //env
    if( is_shell_env(language,tangle_name) ) {
      return (root_dir_path / "etc" / tangle_name ).generic_string();
    }

    //opt
    if( is_shell_option(language,tangle_name) ) {
      return (root_dir_path / "etc" / tangle_name).generic_string();
    }

    //elisp
    if(is_elisp(language) ) {
      std::string p{"share/elisp/site-lisp/"};
      p +=tangle_name; p += ".el";
      return (root_dir_path / p ).generic_string();
    }

    //scheme
    if(is_scheme(language) ) {
      std::string p{"share/scheme/site-lisp/"};
      p += tangle_name;
      if(!tangle_name.ends_with(".scm"))  p += ".scm";
      return (root_dir_path / p ).generic_string();
    }

    //rel files
    if( tangle_name.starts_with("bin") ||
        tangle_name.starts_with("etc") ||
        tangle_name.starts_with("share")) {
      return (root_dir_path / tangle_name ).generic_string();
    }

    // generic file path
    if( tangle_name.starts_with(".") ) {
        const auto what=fmt::format(
            "{}: tangling to relative paths, '.' & '..' is not allowed: {}",
            site_name,
            tangle_name );
        throw std::runtime_error(what);
    } else {

        fs::path file_path;
        wordexp_t word;
        if(wordexp(tangle_name.c_str(),&word,WRDE_NOCMD) == 0 ) {
            file_path = std::string{word.we_wordv[0]};
            wordfree(&word);
        } else {
            file_path = tangle_name;
        }
        return file_path.generic_string();
    }

  }

    std::string
    make_fileid(const std::string& tangle_name, const std::string& language)
    {
        return fmt::format("{}-{}", tangle_name, language );

    }

  ////////////////////////////////////////////////////////////////
  // site_file
  /////////////////////////////////////////////////////////////////
    site_file::site_file( const org_site& site, const org_block& first)
        :_id{},
         _path{},
         _site(site),
         _blocks{first} {

        auto site_name = site.name();
        auto site_dir  = site.directory();
        auto language  = std::string{ first.language };
        auto tangle    = first.get_param("tangle",site_name);
        this->_path    = make_filepath(site_name,tangle,site_dir,language);
        this->_id      = make_fileid(tangle, language );

    }

  bool site_file::_is_exe() const {
    return (this->_blocks.size() > 0 && _blocks[0].is_exe());
  }

  void site_file::add_block( const org_block& block ){
    this->_blocks.push_back(block);
  }


  bool site_file::commit( const site_submap& submap )
  {
    // prepare site directory
    auto site_dir = fs::path(this->path()).parent_path().generic_string();
    std::error_code err;
    if(!ensure_path(site_dir,err)) {
      spdlog::warn("- preparing output path: {}: '{}' ...[FAILED]", site_dir, err.message());
      return false;
    }

    // write blocks
    if(std::ofstream out{this->path(),std::ios::out | std::ios::trunc}; out) {
      // write org blcoks to file
      const auto delm = std::string_view{"\n"};
      for( const auto& block : this->_blocks ) {
        auto rngs = block.content | rv::split(delm);
        for(auto &&rng : rngs ) {

          auto line = std::string_view(&*rng.begin(), std::ranges::distance(rng));
          //handle org-mode escaping of lines that begin with *
          if( trim(line).starts_with(",*") ) {
            auto pos = line.find(",*");
            out << line.substr(0,pos-1);
            line = line.substr(pos+1);
          }
          //handle substitution strings
          {
            auto sub_beg = line.find("<<");
            auto sub_end = line.find(">>");
            if( sub_beg == std::string_view::npos || sub_end == std::string_view::npos ) {
              out << line << std::endl;
              continue;
            }
            if(sub_beg >= 1 ) {
              out << line.substr(0, sub_beg - 1);
            }
            const auto first     = line.data() + sub_beg;
            const auto last      = line.data() + sub_end + 2;
            std::string tangle_name{first,last};
            std::string language{block.language};
            std::string fileid = make_fileid(tangle_name,language);

            const auto& subs = submap.find(fileid);
            if( subs.size() == 0 ) {
              spdlog::warn("missing block substiution: {}", fileid);
              out << line.substr(sub_end + 2);
              continue;
            }

            for( const auto& sub: subs ){
              out << sub.content << std::endl;
            }
            out << line.substr(sub_end + 2);
          }
        }
      }
      out << std::flush;
    } else {
      spdlog::error("- failed to open file for I/O: '{}' ...[FAILED]" );
      return false;
    }

    //write file permissions
    if( this->_is_exe() ) {
      auto exe_perm = fs::perms::owner_all | fs::perms::group_all;
      fs::permissions( this->path() ,exe_perm, fs::perm_options::add,err);
      if(err) {
        spdlog::warn("-failed to set executable permission on file: {}", this->path());
      }
    }
    return true;

  }

}
