// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2025 Aron Gile <aronggile@gmail.com>

#include <iostream>
#include <cstdio>
#include <fstream>
#include <ranges>
#include <filesystem>
#include <iterator>
#include <ranges>
#include <cstdlib>
#include <regex>
#include <string>
#include <memory>
#include <array>
#include <sstream>
#include <spdlog/spdlog.h>
#include <fmt/core.h>
#include "site/site_options.hpp"
#include "config/config_settings.hpp"
#include "org/org_file.hpp"
#include "site/org_site.hpp"

namespace fs = std::filesystem;
namespace r  = std::ranges;
namespace litdoc {

    struct runtime_info
    {

        enum runtime_type
        {
            container,
            shell,
            host
        };

        inline explicit runtime_info(
            const std::string& name,
            runtime_type type,
            const fs::path& opt_file)
            :name(name),
             type(type) ,
             opt_file(opt_file){
        }

        explicit runtime_info(const runtime_info&) = default;

        const std::string  name;
        const runtime_type type;
        const fs::path opt_file;
    };


    void substitute_envstr(
        const org_site& site,
        const runtime_info&,
        std::string& str) {

        const fs::path site_dir{ site.directory() };
        static std::regex env( "\\$\\{([^}]+)\\}" );
        std::smatch match;
        while (std::regex_search(str, match, env)) {

            const char *ptr_var = std::getenv( match[1].str().c_str() );
            if( ptr_var != nullptr ) {
                const std::string var( ptr_var );
                str.replace( match[0].first, match[0].second, var );
            }
            else if( match[1].str() == "LITDOC_ORGDIR" ) {
                const fs::path org_dir = fs::path{ site.source().path() }.parent_path();
                const std::string var{ org_dir.generic_string() };
                str.replace( match[0].first, match[0].second, var);
            }
            else if( match[1].str() == "LITDOC_SITEDIR" ) {
                const std::string var{ site_dir.generic_string() };
                str.replace( match[0].first, match[0].second, var);
            }

        }

    }


    bool needs_container(const fs::path& runtime_opt )  {

        std::ifstream opt_file{runtime_opt.generic_string()};
        if(!opt_file) {
            spdlog::error("failed to open options file: {}", runtime_opt.generic_string());
            std::exit(1);
        }
        std::string line;
        while( std::getline(opt_file,line)) {
            if( line.starts_with("--container") ) {
                return true;
            }
        }
        return false;
    }

    runtime_info get_runtime_info(
        const std::string& runtime,
        const org_site& site) {

        const fs::path site_dir{ site.directory() };
        const fs::path etc_dir = ( site_dir / "etc");
        const fs::path empty_path;

        // spawn custom shell?
        {
            const auto opt_file=(etc_dir / fmt::format("{}.opt", runtime));
            if(fs::exists(opt_file)) {

                if(needs_container(opt_file)) {
                    return runtime_info {
                        runtime,
                        runtime_info::container,
                        opt_file
                    };
                } else {
                    return runtime_info {
                        runtime,
                        runtime_info::shell,
                        opt_file
                    };
                }
            }
        }

        // spawn is_containerized shell?
        {
            const auto opt_file=(etc_dir / "container.opt");
            if(fs::exists(opt_file)) {
                return runtime_info{
                    runtime, runtime_info::container, opt_file};
            }
            const auto env_file=(etc_dir / "container.env");
            if(fs::exists(env_file)){
                return runtime_info{runtime,runtime_info::container,empty_path};
            }
        }

        // inherit host shell?
        {
            const auto opt_file=(etc_dir / "host.opt");
            if(fs::exists(opt_file)) {
                return runtime_info{
                    runtime,
                    runtime_info::shell,
                    opt_file };
            }
        }

        // default
        return runtime_info {
            runtime,
            runtime_info::host,
            empty_path};

    }

    bool write_shell_options(
        const org_site& site,
        const runtime_info& info,
        std::ostringstream& out )  {

        const fs::path site_dir { site.directory() };
        const fs::path etc_dir{ site_dir / "etc" };

        if( !fs::exists(info.opt_file)) {
            spdlog::info("[skipped]: site {} has no shell option file: {}",
                         site.name(),
                         info.opt_file.generic_string());
            return true;
        }
        std::ifstream opt_file{info.opt_file.generic_string()};
        if(!opt_file) {
            spdlog::error(
                "failed to open options file: {}",
                info.opt_file.generic_string());
            return false;
        }

        std::string line;
        while( std::getline(opt_file,line)) {
            if( !line.starts_with("#") ){
                substitute_envstr(site,info,line);
                out << line << " ";
            }
        }

        return true;
    }

    bool write_manifest_options(
        const org_site& site,
        const runtime_info& info,
        std::ostringstream& out) {

        fs::path site_dir { site.directory() };
        fs::path etc_dir{ site_dir / "etc" };

        //if the default environment is a container, we include
        //the default manifest. For a site running directly in
        // the host or in a modified shell in the host, we assume the default
        //manifest or something  equivalent is available.
        if( info.type == runtime_info::container ) {
            fs::path root_path{ site.config().root_directory };
            auto default_manifest = ( root_path / "etc/default.scm" );
            if(!fs::exists(default_manifest) ) {
                spdlog::error("failed to open or read the default manifest: {}",
                              default_manifest.generic_string());
                return false;
            }
            out << " --manifest=" << default_manifest.generic_string();
        }

        //manifest
        {
            auto manifest = ( etc_dir / "manifest.scm") ;
            if( fs::exists(manifest) ) {
                out << " --manifest="  << manifest.generic_string();
            }
        }

        //auto manifest
        {
            auto auto_manifest  = ( etc_dir / "manifest.auto.scm" );
            if( fs::exists(auto_manifest) ) {
                out << " --manifest="  << auto_manifest.generic_string();
            }
        }

        return true;

    }

    bool write_default_profile( std::ostringstream& out ) {
      auto config = config_settings::get_current();
      const fs::path cache_dir{config.cache_directory};
      const auto guix_profile = (cache_dir / "dispatch/guix/default/default");
      out << " --profile="  << guix_profile.generic_string()  << " ";
      return true;
    }


    bool write_profile_options(
        const org_site&  site,
        const runtime_info&,
        std::ostringstream& out ) {
        const fs::path site_dir{ site.directory() };
      const auto guix_dir           = ( site_dir / "guix");
      const auto guix_profile       = ( guix_dir / site.name()/ site.name() );
      out << " --profile="  << guix_profile.generic_string()  << " ";
      return true;
    }

    bool write_loadpath_options(
        const org_site& site,
        const runtime_info&,
        std::ostringstream& out ) {

        const fs::path root_dir{ site.config().root_directory };
        const fs::path site_dir{ site.directory() };
        const auto default_load_dir= ( root_dir / "share/guix/site-lisp");
        //default library path
        if(!fs::exists(default_load_dir)){
            spdlog::error(
                "unable to open or read the default load path: {}",
                default_load_dir.generic_string());
            return false;
        }
        out << " --load-path="  << default_load_dir.generic_string()  << " ";

        //dynamically loaded site directory?
        if(const auto auto_load_dir
           = ( site_dir / "share/scheme/site-lisp");
           fs::exists(auto_load_dir) ) {
            out << " --load-path="  << auto_load_dir.generic_string()  << " ";
        }

        return true;
    }


    bool write_env_options(
        const org_site& site,
        const runtime_info& runtime,
        std::ostringstream& out ) {

        const fs::path site_dir{ site.directory() };
        const fs::path etc_dir{ (site_dir / "etc") };
        auto site_env = (etc_dir / fmt::format("{}.env", runtime.name) );
        if( fs::exists(site_env) ) {
            out << "" << site_env << " ";
        }

        return true;
    }


    auto expose_file(
        const fs::path& src_file,
        const fs::path& dest_file,
        std::ostringstream& out) -> void {
        out << " --expose="
            << src_file.generic_string()
            << "="
            << dest_file.generic_string() ;
    }
    void write_prompt(
        const org_site& site,
        const runtime_info&,
        std::ostringstream& out){

        const fs::path site_dir{ site.directory() };
        fs::path main_sh{ (site_dir / fmt::format("bin/{}.sh", site.name())) };
        out << " PS1="
            << R"(\W@\u)"
            << "[" << site.name() << "]"
            << R"(> )";
    }

    bool write_main_host_options(
        const org_site& site,
        const runtime_info& info,
        std::ostringstream& out){

        const fs::path root_dir{ site.config().root_directory };
        const fs::path site_dir{ site.directory() };
        const fs::path org_dir = fs::path{ site.source().path() }.parent_path();

        out << " -- env "
            << " LITDOC_SITEDIR="      << site_dir.generic_string()
            << " LITDOC_ORGDIR="       << org_dir.generic_string();
        const fs::path env_file {site_dir / "etc" / fmt::format("{}.env", site.name()) };
        if(fs::exists(env_file)) {
            out << " LITDOC_RCFILE=" << env_file.generic_string();
        }
        const fs::path rcfile{ root_dir / "etc/profile" };
        if(!fs::exists(rcfile)){
            spdlog::error("failed to read site profile template file: {}", rcfile.generic_string());
            return false;
        }
        fs::path main_sh{ (site_dir / fmt::format("bin/{}.sh", site.name())) };
        write_prompt(site,info,out);
        out << fmt::format(
                           " bash --rcfile {} -i {} ",
                           rcfile.generic_string(),
            fs::exists(main_sh)? main_sh.generic_string() : std::string{""});

        return true;
    }

    bool write_main_shell_options(
        const org_site& site,
        const runtime_info& info,
        std::ostringstream& out) {

        const fs::path site_dir{ site.directory() };
        const fs::path org_dir = fs::path{ site.source().path() }.parent_path();
        const fs::path root_dir{ site.config().root_directory };

        std::string LITDOC_RCFILE;
        {
            fs::path p = (site_dir / "etc" / fmt::format("{}.env", info.name));
            if(fs::exists(p)) {
                LITDOC_RCFILE = fmt::format(" LITDOC_RCFILE={}",p.generic_string());
            } else {
                LITDOC_RCFILE="";
            }
        }

        out << " -- env "
            << " LITDOC_DIR="          << std::getenv("LITDOC_DIR")
            << " LITDOC_CACHEDIR="     << std::getenv("LITDOC_CACHEDIR")
            << " LITDOC_SITEDIR="      << site_dir.generic_string()
            << " LITDOC_ORGDIR="       << org_dir.generic_string()
            << LITDOC_RCFILE
            << " TERM=xterm-256color ";
        write_prompt(site,info,out);

        //main
        fs::path rcfile = (root_dir / "etc/profile" );
        fs::path main_sh{ (site_dir / fmt::format("bin/{}.sh", site.name())) };
        out << fmt::format(" bash --rcfile {} -i {}",
                           rcfile.generic_string(),
                           fs::exists(main_sh)? main_sh.generic_string() : "");

        return true;

    }

    bool write_main_container_options(
        const org_site& site,
        const runtime_info& info,
        std::ostringstream& out){

        std::vector<std::string> env;
        const fs::path site_dir{ site.directory() };
        const fs::path src_dir{std::getenv("LITDOC_DIR")};
        if(!fs::exists(src_dir)){
            spdlog::error(
                "unable to resolve litdoc-dir path from LITDOC_DIR:{}",
                src_dir.generic_string());
            return false;
        }

        // site.env
        {
            const auto site_env = (site_dir/ "etc" / fmt::format("{}.env", info.name));
            if(fs::exists(site_env)){
                const fs::path _site_env{  fmt::format("/etc/profile.d/{}", site.name()) };
                expose_file(site_env,_site_env,out);
            }
        }

        //litdoc directory
        {

            const fs::path _src_dir{"/opt/litdoc"};
            expose_file(src_dir,_src_dir,out);
            env.push_back( fmt::format("LITDOC_DIR={}",_src_dir.generic_string()));
        }

        //site directory
        {
            const fs::path _site_dir{fmt::format("/opt/sites/{}",site.name())};
            expose_file(site_dir,_site_dir,out);
            env.push_back( fmt::format("LITDOC_SITEDIR={}",_site_dir.generic_string()));
        }

        //main
        std::string main_sh;
        {
            fs::path p{ (site_dir / fmt::format("bin/{}.sh", site.name())) };
            if(fs::exists(p)) {
                const fs::path _p{fmt::format("/opt/sites/{}/bin/{}.sh ", site.name(), site.name())};
                expose_file(p,_p,out);
                main_sh = p.generic_string();
            }
        }

        //rcfile
        std::string rcfile{"/opt/litdoc/src/etc/profile"};

        //define variables
        {
            out << " -- env TERM=xterm-256color "
                << "LITDOC_RUNTIME_TYPE=container";
            write_prompt(site,info,out);
            for( const std::string& env_var : env ){
                out << env_var << " ";
            }
        }

        out << fmt::format(
            " bash --rcfile {} -i {}",
            rcfile,
            main_sh);

        return true;

    }

    site_options::site_options( const litdoc::org_site& site )
        :site(site) {
    }


    auto site_options::to_profile_config(
        const std::string& runtime,
        std::ostringstream& out) const -> bool {
        const auto info {
            get_runtime_info(runtime,this->site)
        };
        return write_shell_options(site,info,out) &&
            write_loadpath_options(site,info,out) &&
            write_manifest_options(site,info,out) &&
            write_profile_options(site,info,out);
    }


    auto site_options::to_shell_config(
        const std::string& runtime,
        std::ostringstream& out) const -> bool {

        const runtime_info info{
            get_runtime_info(runtime,this->site)
        };
        // When using guix shell --container with --emulate-fhs
        // option, Guix will expand the --emulate-fhs option as
        // a package to be included in the container environment.
        // This feature is not compatible with the --profile
        // option and will result in cryptic error message. As a
        // workaround we use --manifest option to compose a profile on
        // the fly.
        if( info.type == runtime_info::container) {
            out << "--container --emulate-fhs ";
            return write_shell_options(site,info,out) &&
                write_manifest_options(site,info,out) &&
                write_loadpath_options(site,info,out) &&
                write_main_container_options(site,info,out);
        } else if( info.type == runtime_info::shell ) {
            out <<  " --check ";
            return write_shell_options(site,info,out) &&
                write_profile_options(site,info,out)  &&
                write_loadpath_options(site,info,out) &&
                write_main_shell_options(site,info,out);
        } else {

            return write_profile_options(site,info,out)  &&
                write_loadpath_options(site,info,out)    &&
                write_main_host_options(site,info,out);
        }
    }



}//namespace
