// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include <openssl/sha.h>
#include <openssl/evp.h>

#include <filesystem>
#include <iostream>
#include <fstream>
#include <optional>
#include <memory>
#include <iterator>

#include <spdlog/spdlog.h>
#include "fs/ensure_path.hpp"
#include "site/site_fingerprint.hpp"
#include "config/config_settings.hpp"
#include "org/org_file.hpp"
#include "site/org_site.hpp"


namespace fs = std::filesystem;
namespace litdoc {

  ////////////////////////////////////////////////////////////////////////
  // utilities
  ////////////////////////////////////////////////////////////////////////
  template<typename T>
  auto sha256( T first, T last) -> std::optional<std::string>
  {
    std::shared_ptr<EVP_MD_CTX> context{ EVP_MD_CTX_new(), EVP_MD_CTX_free };
    if(!context) return std::nullopt;
    EVP_MD* algo = EVP_MD_fetch(nullptr,"SHA256",nullptr);
    if(algo == nullptr ) return std::nullopt;
    if( EVP_DigestInit_ex(context.get(), algo, nullptr) != 1)
      return std::nullopt;;
    const std::string_view input(first,last);
    if(EVP_DigestUpdate(context.get(),
                        reinterpret_cast<const void*>(input.data()),
                        static_cast<std::size_t>(input.size())) != 1 ){

      return std::nullopt;
    }
    std::array<unsigned char,  SHA256_DIGEST_LENGTH > md;
    if( EVP_DigestFinal_ex(context.get(), md.data(), nullptr) != 1){
      return std::nullopt;
    }
    std::stringstream stream;
    stream << std::hex << std::setfill('0');
    for( const auto& byte : md ) {
      stream << std::setw(2) << static_cast<unsigned int>(byte);
    }
    return stream.str();

  }

  auto sha256_write(const std::string& hash_file, const std::string_view data) -> bool
  {
    auto const first = std::begin(data);
    auto const last  = std::end(data);
    auto md = sha256(first, last);
    if(!md) {
      spdlog::debug("* [fingerprint]: failed to hash file: {}", hash_file);
      return false;
    }

    const auto parent_dir = fs::path{ hash_file }.parent_path();
    std::error_code err;
    if(!ensure_path(parent_dir,err)){
      spdlog::error("failed to create directory: {} with error:\n {}",
                    parent_dir.c_str(),
                    err.message());
      return false;
    }

    if(std::ofstream out_file{hash_file,std::ofstream::out | std::ofstream::trunc}; out_file){
      out_file.write( (*md).c_str(), md->size());
      auto result =  out_file.good();
      if(!result) spdlog::debug("* [fingerprint]: failed to write hash file: {}", hash_file);
      return result;
    }

      spdlog::debug("* [fingerprint]: failed to open file: {}", hash_file);
    return false;
  }

  auto sha256_check( const std::string& hash_file, const std::string_view data ) -> bool
  {

    auto const actual_first = std::begin(data);
    auto const actual_last  = std::end(data);
    auto actual = sha256(actual_first, actual_last);
    if(!actual) return false;
    if(std::ifstream in_str{hash_file,std::ofstream::in}; in_str) {
      using iterator_type = std::istreambuf_iterator<char>;
      auto expected_first = iterator_type{in_str};
      auto expected_last  = iterator_type{};
      auto expected = std::string(expected_first,expected_last);
      if(!in_str) return false;
      auto actual_size = actual->size();
      const auto& actual_ref = *actual;
      for( std::string::size_type index = 0; index < actual_size ; index++ ) {

        if( actual_ref[index] != expected[index] ) {
          return false;
        }

      }

      return true;
    }
    return false;
  }

  auto get_hash_file( const litdoc::org_site& site ) -> fs::path {

    fs::path source_path{ site.source().path() };
    fs::path hash_file_path = fs::path{ site.directory() } / source_path.replace_extension(".org.sha256").filename();
    return hash_file_path;
  }

  /////////////////////////////////////////////////////////////////////
  // site_fingerprint
  /////////////////////////////////////////////////////////////////////
  site_fingerprint::site_fingerprint( const litdoc::org_site& site )
  :site(site)
  {

  }

  auto site_fingerprint::check( ) const -> bool
  {
    const auto& data = site.source().content();
    fs::path hash_file{ get_hash_file( this->site ) };
    if(!fs::exists(hash_file)) {
      return false;
    }

    if(!sha256_check(hash_file,data) ){
      return false;
    }

    return true;
  }


  auto site_fingerprint::build() -> bool
  {

    fs::path hash_file{get_hash_file(this->site)};
    if(!sha256_write(hash_file, site.source().content())) {
      spdlog::error("[fingerpint({})]: failed to write site fingerprint to file:{} [ERROR]",
                    site.name(), hash_file.generic_string());
      return false;
    }
    spdlog::info("added fingerprint site<{}>: {}", site.name(), hash_file.generic_string());
    return true;
  }


}//namespace
