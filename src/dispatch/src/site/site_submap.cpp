// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "site/site_submap.hpp"
#include "site/org_site.hpp"
#include <fmt/core.h>
#include <algorithm>
#include <ranges>


namespace rv = std::ranges::views;
namespace litdoc {

  void site_submap::add_blocks( const std::vector<org_block>& blocks )
  {
    const auto is_sub_block = [ ]( const auto& block ) {
      return block.is_enabled()
        && block.is_src()
        && block.is_sub();
    };
    const org_site& site{ _site };
    std::unordered_map<key_type,value_type>& map{ this->_map };
    auto sub_blocks = blocks | rv::filter(is_sub_block);
    std::ranges::for_each( sub_blocks , [ &site, &map ]( const org_block& sub_block ) {

      std::string tangle_name = sub_block.get_param("tangle",site.name());
      std::string language{ sub_block.language };
      std::string sub_name{  fmt::format("{}-{}", tangle_name, language)};
      auto pos = map.find( sub_name );
      if( pos != map.end() ){
        std::vector<org_block>& sub_blocks = pos->second;
        sub_blocks.push_back(sub_block);
      } else {
        std::vector<org_block> sub_blocks{ sub_block };
        map.insert( std::make_pair(sub_name,sub_blocks ) );
      }

    });

  }


  const std::vector<org_block>& site_submap::find( const key_type& sub_name ) const {
    static std::vector<org_block> EMPTY_BLOCKS;
    const auto pos = this->_map.find(sub_name);
    if( pos == _map.end()) {
      return EMPTY_BLOCKS;
    }
    return pos->second;
  }



}
