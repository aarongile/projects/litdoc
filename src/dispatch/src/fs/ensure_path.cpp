// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "fs/ensure_path.hpp"
#include <filesystem>

namespace litdoc {

  bool ensure_path(std::string const & filepath, std::error_code & err)
  {
    if(std::filesystem::exists(filepath,err)) {
      err.clear();
      return true;
    }

    std::filesystem::create_directories(filepath,err);
    if( err ) {
      return false;
    }

    err.clear();
    return true;
  }

}
