// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "fs/trim.hpp"

namespace litdoc {

  auto trim( std::string_view s) -> std::string_view  {
    auto first = s.find_first_not_of(' ');
    if (std::string_view::npos == first) {
      return s;
    }
    auto last = s.find_last_not_of(' ');
  return s.substr(first, (last - first + 1));
 }

}
