// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include <iostream>
#include <algorithm>
#include <cstdlib>

#include <spdlog/spdlog.h>
#include "spdlog/sinks/stdout_color_sinks.h"

#include "arg-parser"
#include "config/config_settings.hpp"
#include "org/org_file.hpp"
#include "site/org_site.hpp"
#include "site/site_options.hpp"

enum  exit_code : int {
  exit_code_ok = 0x0,
  exit_code_arg_error,
  exit_code_file_error,
  exit_code_build_error
};

void init_logger( const litdoc::config_settings& config ){
  auto log_sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
  auto level = config.enable_verbose()?
    spdlog::level::info :
    spdlog::level::warn;
  log_sink->set_level(level);
  auto log = std::make_shared<spdlog::logger>("litdoc");
  log->sinks().push_back(log_sink);
  spdlog::set_default_logger(log);
}

void print_usage() {
  std::cerr
    << "usage:" << std::endl
    << "- dispatch find    ORG [OPTIONS] " << std::endl
    << "- dispatch ls-deps ORG [OPTIONS] " << std::endl
    << "- dispatch tangle  ORG [OPTIONS]"  << std::endl
    << "- dispatch options ORG [OPTIONS]"  << std::endl;
}


constexpr unsigned int cmd_idx(const std::string_view str ) noexcept {
  uint32_t hash = 5381;
  for(char ch : str)
    hash = ((hash << 5) + hash) + static_cast<unsigned char>(ch);
  return hash;
}

void cmd_find( const std::string_view filename, const litdoc::config_settings& config ) {
  auto doc_path = litdoc::org_find(filename,config);
  if(!doc_path) {
    spdlog::error("unable to find org filename: {}", filename);
    std::exit(exit_code_file_error);
  }
  std::cout << *doc_path << std::endl;
}

void cmd_tangle( const std::string_view filename, const litdoc::config_settings& config ) {
  auto file_path = litdoc::org_find(filename,config);
  if(!file_path) {
    spdlog::error("unable to find org file: {}", filename);
    std::exit(exit_code_file_error);
  }
  litdoc::org_file source{ *file_path };
  litdoc::org_site site{ source, config };
  if(auto status = site.build(); status == litdoc::build_status::ERROR ){
    spdlog::error("failed to tangle site");
    std::exit(exit_code_build_error);
  }

}

void cmd_lsdeps( const std::string_view filename , const litdoc::config_settings& config ) {

  auto file_path = litdoc::org_find(filename,config);
  if(!file_path) {
    spdlog::error("unable to find org file: {}", filename);
    std::exit(exit_code_file_error);
  }
  litdoc::org_file doc{ *file_path };
    std::vector<std::string> deps;
    doc.getdeps_full(deps);
    for(const auto& dep : deps ) {
      std::cout << dep << " ";
    }
    std::cout << std::endl;
  }

void cmd_options(
    const std::string_view filename ,
    const litdoc::config_settings& config) {

    auto file_path = litdoc::org_find(filename,config);
    if(!file_path) {
        spdlog::error("unable to find org file: {}", filename);
        std::exit(exit_code_file_error);
    }
    litdoc::org_file source{ *file_path };
    litdoc::org_site site{ source, config };
    litdoc::site_options opts{site};
    std::ostringstream out;
    std::string runtime;
    out << "profile config:\n";
    if(!opts.to_profile_config(runtime,out)){
        spdlog::error("failed to list options for: {}", filename);
        std::exit(exit_code_file_error);
    }
    out << "\n shell config:\n";
    if(!opts.to_shell_config(runtime,out)){
        spdlog::error("failed to list options for: {}", filename);
        std::exit(exit_code_file_error);
    }

    std::cout << out.str() << std::endl;
}

int main( int argc, const char**argv) {

  try {

    auto args = litdoc::cli::args_positional(argc,argv);
    if( args.size() < 3 ) {
      print_usage();
      return exit_code_arg_error;
    }

    //config
    auto config = litdoc::config_settings::get_current();
    init_logger(config);

    auto cmd = args[1];
    spdlog::info("running command: {}", cmd);
    switch( cmd_idx(cmd) ) {

    case cmd_idx("find"): {
      const std::string_view filename{ args[2] };
      cmd_find(filename,config);
    } break;

    case cmd_idx("tangle"): {
      const std::string_view filename{ args[2] };
      cmd_tangle(filename,config);
    } break;
    case cmd_idx("options"): {
      const std::string_view filename{ args[2] };
      cmd_options(filename,config);
    } break;
    case cmd_idx("lsdep"): {
      const std::string_view filename{ args[2] };
      cmd_lsdeps(filename,config);
    } break;
    default: {
      spdlog::error("unsupported command: {}", cmd);
      std::exit(exit_code_arg_error);
    } break;

    };

  } catch( const std::exception& ex) {
    spdlog::error("[main]: internal error: {}", ex.what());
    return exit_code_build_error;
  }

  return exit_code_ok;
}
