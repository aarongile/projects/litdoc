#include <string>
#include <string_view>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <spdlog/spdlog.h>
#include "spdlog/sinks/stdout_color_sinks.h"
#include "config/config_settings.hpp"
#include "org/org_file.hpp"
#include "site/org_site.hpp"
#include "site/site_options.hpp"

extern "C" {

		void init_module(int verbosity){
				//verbosity
				if( (verbosity < SPDLOG_LEVEL_TRACE) ||
						(verbosity > SPDLOG_LEVEL_OFF ) ) {
						std::cerr << "[ERROR]: invalid verbosity level: "
											<< verbosity << std::endl;
						std::cerr << "verbosity levels:\n"
											<< "0 → trace-level debugging (most detailed)\n"
											<< "1 → debug messages\n"
											<< "2 → informational messages(default)\n"
											<< "3 → warning messages\n"
											<< "4 → error messages\n"
											<< "5 → critical error messages\n"
											<< "6 → silent(no logging)";
						std::exit(1);
				}
				//configure spdlog log level
				auto log_sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
				auto level = static_cast<spdlog::level::level_enum>(verbosity);
				log_sink->set_level(level);
				auto log = std::make_shared<spdlog::logger>("litdoc");
				log->sinks().push_back(log_sink);
				spdlog::set_default_logger(log);
		}

		void log_info( const char* message ) {
				std::string_view m{message};
				spdlog::info(m);
		}

		void log_warn( const char* message ) {
				std::string_view m{message};
				spdlog::warn(m);
		}
		void log_error( const char* message ) {
				std::string_view m{message};
				spdlog::error(m);
		}

		int dispatch_version() {
				std::cout << "version 0.0.1" << std::endl;
				return 0;
		}

		int dispatch_find( const char* ptr_doc ) {

				const std::string_view  doc_name{ptr_doc};
				auto config = litdoc::config_settings::get_current();
				auto doc_path = litdoc::org_find(doc_name,config);
				if(!doc_path) {
						spdlog::error("unable to find org filename: {}",doc_name);
						return 1;
				}
				std::cout << *doc_path << std::endl;
				return 0;
		}

		int dispatch_lsdeps( const char* ptr_doc ) {
				const std::string_view docname{ptr_doc};
				auto  config  = litdoc::config_settings::get_current();
				auto doc_path = litdoc::org_find(docname,config);
				if(!doc_path) {
						spdlog::error("unable to find org filename: {}",docname);
				return 1;
				}
				litdoc::org_file doc{ *doc_path };
				std::vector<std::string> deps;
				doc.getdeps_full(deps);
				for(const auto& dep : deps ) {
						std::cout << dep << " ";
				}
				return 0;
		}

		int dispatch_build( const char* ptr_doc ) {
				const std::string_view docname{ptr_doc};
				auto config = litdoc::config_settings::get_current();
				auto doc_path = litdoc::org_find(docname,config);
				if(!doc_path) {
						spdlog::error("unable to find org filename: {}",docname);
				return 1;
    }
				litdoc::org_file source{ *doc_path };
				litdoc::org_site site{ source, config };
				auto status = site.build();
				return static_cast<int>(status);
		}

		int dispatch_profile_options(
				const char* ptr_doc,
				const char* ptr_runtime) {

				const std::string_view docname{ptr_doc};
				auto  config  = litdoc::config_settings::get_current();
				auto doc_path = litdoc::org_find(docname,config);
				if(!doc_path) {
						spdlog::error("unable to find org file: {}",docname);
						return 1;
				}
				litdoc::org_file source{ *doc_path };
				litdoc::org_site site{ source, config };
				litdoc::site_options opts{site};

				std::ostringstream out;
				const std::string runtime{ptr_runtime};
				if(!opts.to_profile_config(runtime,out)){
						std::cerr << "failed to list options for: " << docname;
						return 1;
				}
				std::cout << out.str() << std::endl;
				return 0;
		}

		int dispatch_runtime_options(
				const char* ptr_doc,
				const char* ptr_runtime ) {
				const std::string_view docname{ptr_doc};
				auto  config  = litdoc::config_settings::get_current();
				auto doc_path = litdoc::org_find(docname,config);
				if(!doc_path) {
						spdlog::error("unable to find org file: {}",docname);
						return 1;
				}
				litdoc::org_file source{ *doc_path };
				litdoc::org_site site{ source, config };
				litdoc::site_options opts{site};

				std::ostringstream out;
				const std::string runtime{ptr_runtime};
				if(!opts.to_shell_config(runtime,out)){
						std::cerr << "failed to list options for: " << docname;
						return 1;
				}
				std::cout << out.str() << std::endl;
				return 0;
		}

}
