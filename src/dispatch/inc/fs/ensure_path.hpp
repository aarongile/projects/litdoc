// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once
#include <string>
#include <system_error>

namespace litdoc {

  bool ensure_path( const std::string& filepath, std::error_code & err);

}
