// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include <string_view>

namespace litdoc {

  auto trim( std::string_view s) -> std::string_view;

}
