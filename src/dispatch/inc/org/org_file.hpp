// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include <memory>
#include <optional>
#include <string>
#include <string_view>

namespace litdoc {

    struct org_file;
    struct config_settings;

    auto org_find(const std::string_view name,
                  const config_settings& config
                  ) -> std::optional<std::string>;



  struct org_file {

    explicit org_file( const std::string& path );

    auto content( ) const -> std::string_view;
    auto name() const -> const std::string&;
    auto path() const -> const std::string&;

    auto write_sha256( const std::string& hash_file ) const -> bool;
    auto check_sha256( const std::string& hash_file ) const -> bool;

    auto getdeps( std::vector<std::string>& deps ) const -> void;
    auto getdeps_full(std::vector<std::string>& deps ) const -> void;

    ~org_file()=default;
    org_file()=delete;
    org_file( const org_file&) =default;
    org_file( org_file&&)=default;
    org_file& operator=( const org_file&)=default;

  private:
    struct impl;
    std::shared_ptr<impl> pimpl;
  };


}
