// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include <memory>
#include <vector>
#include "query/org_block.hpp"

namespace litdoc {

  struct org_file;
  struct org_block_query {

    using result_type = std::vector<org_block>;

    explicit org_block_query( );
    auto operator()( const org_file& source ) -> result_type;

  private:

    struct impl;
    std::shared_ptr<impl> pimpl;

  };




}
