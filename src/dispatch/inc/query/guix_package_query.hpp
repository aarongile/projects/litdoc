// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include <string_view>
#include <memory>
#include <vector>

namespace litdoc {

  struct guix_package {

    std::string_view name;
    std::string_view version;

  };

  struct guix_package_query {
    explicit guix_package_query( );
    bool operator()(std::string_view content, std::vector<guix_package>& packages) const;

  private:
    struct impl;
    std::shared_ptr<impl> pimpl;

  };


}
