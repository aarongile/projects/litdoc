// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include <string_view>
#include <string>
#include <unordered_map>
#include <vector>

namespace litdoc {


  struct org_block {

      using value_type = std::string_view;
      using key_type   = std::string_view;


    inline auto is_src() const -> bool
    {
      return ( this->type == std::string_view{"src"} );

    }

    inline auto is_sub() const -> bool
    {
      const auto tangle = get_param("tangle","");
      return tangle.starts_with("<<");
    }

    inline auto is_exe() const -> bool
    {
      const auto shebang = get_param("shebang","");
      if( shebang.starts_with("#!") )
        return true;
      else if( this->content.starts_with("#!") )
        return true;
      return false;
    }

    inline auto is_enabled() const -> bool
    {
      const auto tangle = get_param("tangle","");
      return (tangle.empty() || (tangle != "none"));
    }

    inline auto get_param(const std::string& key , const std::string_view default_value) const -> std::string
    {
      auto pos = this->parameters.find(key);
      if(pos != this->parameters.end())
        return std::string(pos->second);
      else
        return std::string(default_value);
    }

    value_type type;
    value_type language;
    value_type content;
    std::unordered_map<value_type, value_type> parameters;


  };


}
