// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once
#include <string>
#include <vector>

namespace litdoc {


  struct config_settings {


    bool enable_verbose() const;
    bool enable_color;

    std::string  cache_directory;
    std::string  root_directory;
    std::vector<std::string> search_paths;

    std::string  version() const;

    static config_settings get_current();

    config_settings( const config_settings& other ) =default;
    config_settings( config_settings&& )=default;
    config_settings& operator=( const config_settings& rhs) =default;
    ~config_settings()=default;

  private:

    config_settings() =default;

  };

}
