// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once
#include <vector>
#include <unordered_map>
#include <string>

namespace litdoc {



  struct org_file;
  struct config_settings;
  enum class [[nodiscard("check build status before discarding")]]
  build_status {
    OK,
    SKIPPED,
    ERROR
  };

  struct org_site {

    explicit org_site(const org_file& source, const config_settings& config);
    inline auto name() const -> const std::string& { return this->_name; }
    inline auto directory() const -> const std::string& { return this->_dir; }
    inline auto source() const -> const org_file&  { return this->_source; }
    inline auto config() const -> const config_settings& { return this->_config; }

    auto build( ) -> build_status;

  private:
    const org_file&        _source;
    const config_settings& _config;
    std::string    _name;
    std::string    _dir;
  };


}
