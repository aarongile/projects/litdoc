// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include <vector>
#include <string>
#include <fstream>
#include "query/org_block.hpp"


namespace litdoc {

  struct site_submap;
  struct org_site;

  std::string
  make_fileid(const std::string& tangle_name,const std::string& language);

  struct site_file {

    explicit
    site_file(const org_site& site, const org_block& first);
    inline const std::string& id() const { return _id; }
    inline const std::string& path() const { return _path; }

    void add_block( const org_block& block);
    bool commit( const site_submap& submap);

  private:

    bool _is_exe() const;

    std::string _id;
    std::string _path;
    const org_site&   _site;
    std::vector<org_block>  _blocks;
  };


}
