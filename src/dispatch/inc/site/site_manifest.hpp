// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once
#include <vector>
#include "query/org_block.hpp"

namespace litdoc {

  struct org_site;

  struct site_manifest {

    explicit site_manifest( const org_site& site );
    bool build( const std::vector<org_block>& blocks );

  private:

    const org_site& _site;

  };


}
