// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2025 Aron Gile <aronggile@gmail.com>

#pragma once

#include <string>
#include <sstream>

namespace fs = std::filesystem;
namespace litdoc {

    struct org_site;
    struct site_options {
        auto to_profile_config(
            const std::string& runtime,
            std::ostringstream& out ) const -> bool;
        auto to_shell_config(
            const std::string& runtime,
            std::ostringstream& out ) const -> bool;


        explicit site_options( const org_site& site);

        const org_site& site;

    };


}//namespace
