// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include "query/org_block.hpp"
#include "site/site_file.hpp"

namespace litdoc {

  struct site_filemap
  {
    inline explicit site_filemap( const org_site& site )
      :_site(site),
       _map{}{
    }

    auto build( const std::vector<org_block>& blocks ) -> bool;

  private:

    void _add_blocks( const std::vector<org_block>& blocks);
    void _rmv_sitedir();
    void _add_symbolic_links();
    const org_site& _site;
    std::unordered_map<std::string,site_file> _map;
  };



}
