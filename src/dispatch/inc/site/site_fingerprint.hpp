// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include "query/org_block.hpp"
#include <string_view>
#include <vector>

namespace litdoc {

  struct org_site;
  struct site_fingerprint {
    explicit site_fingerprint( const org_site& site);
    auto check( ) const -> bool;
    auto build( ) -> bool;
    const org_site& site;
  };

}//namespace
