// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once
#include <string>
#include <unordered_map>
#include "query/org_block.hpp"

namespace litdoc {


  struct org_site;
  struct site_submap {

    using key_type   = std::string;
    using value_type = std::vector<org_block>;

    inline explicit site_submap( const org_site& site )
      :_site(site),_map{ } {
    }
    inline std::size_t size() const {
      return _map.size();
    }
    void add_blocks( const std::vector<org_block>& blocks );
    const std::vector<org_block>& find( const key_type& sub_name ) const;

  private:
    const org_site& _site;
    std::unordered_map<key_type,value_type> _map;

  };


}
