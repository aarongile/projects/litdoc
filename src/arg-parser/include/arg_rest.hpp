/// SPDX-License-Identifier: GPL-3.0-or-later
/// Copyright © 2024 Aron Gile <aronggile@gmail.com>


#pragma once

#include <vector>
#include <string>
#include <span>
#include <ranges>

namespace litdoc::cli {

  auto args_rest(int argc, const char* argv[]) -> std::vector< std::string_view >;
}
