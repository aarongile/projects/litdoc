/// SPDX-License-Identifier: GPL-3.0-or-later
/// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#pragma once

#include <vector>

namespace litdoc::cli {

  struct option {

    using name_type   = std::string;
    using value_type  = std::optional< std::string >;

    name_type  name;
    value_type  value;


    explicit option( const std::string_view& name );
    explicit option(const std::string_view& name, const std::string_view& value );

    option() = default;
    option(const option&) =default;
    ~option() = default;
    option& operator=( const option& ) = default;
    option& operator=( option&& ) =default;



  };

  auto args_optional(int argc , const char* argv[]) -> std::vector<option>;
  auto find_option(const std::vector<option>& options, std::string_view name) -> std::optional<option>;
  auto has_option( const std::vector<option>& options, std::string_view name) -> bool;


}
