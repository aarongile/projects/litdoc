/// SPDX-License-Identifier: GPL-3.0-or-later
/// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <string_view>
#include "doctest/doctest.h"
#include "arg-parser"

namespace cli = litdoc::cli;

TEST_CASE("Positional Arguments")
{
  SUBCASE("Basic Cases") {

    std::vector<const char*> argv({
          "command",
          "--opt0", "--opt1=opt1value",
          "arg0", "arg1",
          "--", "rest0", "rest1"
      });
    auto args = litdoc::cli::args_positional(argv.size(),argv.data());
    REQUIRE_EQ( args.size() ,3);
    REQUIRE_EQ( args[0] , "command");
    REQUIRE_EQ( args[1] , "arg0");
    REQUIRE_EQ( args[2] , "arg1");
  }

  SUBCASE("No Arguments") {

    std::vector<const char*> argv({});
    auto args = litdoc::cli::args_positional(argv.size(),argv.data());
    REQUIRE_EQ( args.size() ,0);

  }

  SUBCASE("Optional Arguments Only") {

    std::vector<const char*> argv({
        "--option1=1", "-op2=2", "--flag", "-f"
      });
    auto args = litdoc::cli::args_positional(argv.size(),argv.data());
    REQUIRE_EQ( args.size() ,0);

  }

  SUBCASE("Rest Args Only") {

    std::vector<const char*> argv({
        "--", "rest1", "rest2", "--rest" ,"-r"
      });
    auto args = litdoc::cli::args_positional(argv.size(),argv.data());
    REQUIRE_EQ( args.size() ,0);

  }
}


TEST_CASE("Optional Arguments")
{
  SUBCASE("Basic Cases") {

    std::vector<const char*> argv({
        "command",
        "--flag", "-f", "--key=value", "-k=value", "-empty-key=",
        "arg0", "arg1",
        "--", "rest0", "rest1"
      });
    auto opts = litdoc::cli::args_optional(argv.size(),argv.data());
    REQUIRE_EQ( opts.size() ,5);
    REQUIRE_EQ( opts[0].name , "flag");
    REQUIRE_EQ( opts[1].name , "f");

    //--key=value
    REQUIRE_EQ( opts[2].name , "key");
    REQUIRE_EQ( *opts[2].value , "value");

    //-k=value
    REQUIRE_EQ( opts[3].name , "k");
    REQUIRE_EQ( *opts[3].value , "value");

    //-empty-key=e
    REQUIRE_EQ( opts[4].name , "empty-key");
    REQUIRE_EQ( *opts[4].value , "");

    //find by argument name
    auto r = litdoc::cli::find_option(opts,"key");
    REQUIRE( r->name  == "key" );
    REQUIRE( r->value == "value" );

    r = litdoc::cli::find_option(opts,"undefined");
    REQUIRE( r.has_value( ) == false);
    REQUIRE( litdoc::cli::has_option(opts,"f") );
  }

}

TEST_CASE("Rest Arguments")
{
  SUBCASE("Basic Cases") {

    std::vector<const char*> argv({
        "command",
        "arg0", "arg1",
        "--", "rest0", "rest1"
      });

    auto rest = litdoc::cli::args_rest(argv.size(),argv.data());
    REQUIRE_EQ( rest.size(),2);
    REQUIRE_EQ( rest[0], "rest0");
    REQUIRE_EQ( rest[1], "rest1");
  }
}
