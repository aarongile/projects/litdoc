/// SPDX-License-Identifier: GPL-3.0-or-later
/// Copyright © 2024 Aron Gile <aronggile@gmail.com>

#include "arg_positional.hpp"

namespace litdoc::cli {

  constexpr auto to_string_view(const char* value ) -> std::string_view {
    return std::string_view(value);
  }

  constexpr auto is_arg( const std::string_view& value ) -> bool {
    return (value != "--") && !value.empty();
  };

  constexpr auto is_positional( const std::string_view& value) -> bool {
    return !value.starts_with("-");
  };


  auto args_positional( int argc, const char* argv[] ) -> std::vector<std::string_view> {

    auto r = std::span(argv,argc)
      | std::ranges::views::transform(to_string_view)
      | std::ranges::views::take_while(is_arg)
      | std::ranges::views::filter(is_positional);

    std::vector<std::string_view> out;
    for( auto value : r )
      out.push_back(value);

    return out;
  }




}
