/// SPDX-License-Identifier: GPL-3.0-or-later
/// Copyright © 2024 Aron Gile <aronggile@gmail.com>


#include <string>
#include <ranges>
#include <span>
#include "arg_optional.hpp"

namespace litdoc::cli {

  option::option( const std::string_view& name )
    :name(name),
     value() {

  }

  option::option(const std::string_view& name, const std::string_view& value )
    :name(name),
     value(value) {
  }

  constexpr auto to_string_view( const char* value ) -> std::string_view {
    return std::string_view(value);
  }

  constexpr auto is_arg( const std::string_view& value) -> bool {
    return (value != "--");
  }

  constexpr auto is_optional( const std::string_view& value) -> bool {
    return !value.starts_with("---") &&
      (value.starts_with("-") || value.starts_with("--"));
  }

  constexpr auto is_not_eq_sign( char value ) {
    return (value != '=');
  }

  constexpr auto is_dash_sign( char value ) {
    return (value == '-');

  }

  auto to_option( const std::string_view& value ) -> option {


    std::string name;
    {
      auto chars = value
        | std::ranges::views::take_while(is_not_eq_sign)
        | std::ranges::views::drop_while(is_dash_sign);
      for( auto ch : chars) {
        name += ch;
      }

    }

    std::string val;
    {
      auto chars = value
        | std::ranges::views::drop_while(is_not_eq_sign)
        | std::ranges::views::drop(1);
      for( auto ch : chars ) {
        val += ch;
      };

    }

    const option opt( std::move(name), std::move(val) );
      return opt;
    }


  auto args_optional(int argc, const char* argv[]) -> std::vector<option>
  {

    auto r = std::span(argv,argc)
      | std::views::transform(to_string_view)
      | std::views::take_while(is_arg)
      | std::views::filter(is_optional)
      | std::views::transform(to_option);

    std::vector<option> out;
    for( auto opt : r ) {
      out.push_back(opt);
    };
    return out;

  }

  auto find_option(
                   const std::vector<option>& options,
       std::string_view name) -> std::optional<option> {

    auto compare_name = [&name]( const option& opt ) {
      return ( opt.name == name );
    };
    const auto first = options.begin();
    const auto last  = options.end();
    const auto pos = std::find_if(first,last, compare_name);
    if(pos != last) {
      return std::optional<option>(*pos);
    }

    return std::optional<option>();

  }

  auto has_option( const std::vector<option>& options, std::string_view name) -> bool {
    auto opt = find_option(options,name);
    return opt.has_value();
  }

}
