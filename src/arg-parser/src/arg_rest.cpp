/// SPDX-License-Identifier: GPL-3.0-or-later
/// Copyright © 2024 Aron Gile <aronggile@gmail.com>


#include "arg_rest.hpp"

namespace litdoc::cli {

  constexpr auto to_string_view(const char* value ) -> std::string_view {
    return std::string_view(value);
  }

  constexpr auto is_arg( const std::string_view& value ) -> bool {
    return (value != "--");
  }

  constexpr auto is_positional( const std::string_view& value) -> bool {
    return !value.starts_with("-");
  }


  auto args_rest( int argc, const char* argv[] ) -> std::vector<std::string_view> {

    auto r = std::span(argv,argc)
             | std::ranges::views::transform(to_string_view)
             | std::ranges::views::drop_while(is_arg)
             | std::ranges::views::drop(1);

    std::vector<std::string_view> out;
    for( auto value : r ) out.push_back(value);

    return out;
 }




}
