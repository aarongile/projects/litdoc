# LitDoc

`LitDoc` is a GNU Guix extension that facilitates literate programming. It currently
supports documents formatted using Emacs org-mode file format. Further, a `LitDoc`
document can contain one or more specialized source blocks useful for managing per
document software dependencies and providing shell based document interaction.
## Featuer Summary
- parse an org document containing `litdoc` source blocks and setup a Guix shell
  environment that is automatically managed using an isolated Guix profile.
- run shell scripts specified in the org document directly from the command line
- manage dependency between org files

## Requirements

 - <a href="https://git-scm.com">Git</a>
 - <a href="https://guix.gnu.org/manual/en/html_node/Installation.html">Gnu/Guix Package Manager</a>

## Installing

To install `LitDoc` first clone this repository to some directory of
choice,`~/opt/litdoc` for example.

``` sh
mkdir -p ~/opt
cd ~/opt
git clone https://gitlab.com/aarongile/projects/litdoc.git
```
Build the project from source using the <code>run</code> command,
```sh
./run build
```
Finally source the environment setup file `.rcfile` from a shell environment of
your choice. For example, bash users can add the line below to their `~/.bashrc`,
``` sh
source "~/opt/litdoc/.rcfile"
```
Optionally define `LITDOC_PATH` and point it to one
or more colon `:` seperated search paths `LitDoc` should search
for when looking up documents.

If all goes well the `LitDoc` command should be available in
your shell. Try running `guix ld help` to confirm the installation.

## Example: Hello World in Python

Create a `hello.org` file, edit its content to read,
``` org
#+title: hello world
* Hello World with LitDoc!!

This is a hello world literate document using python.

A =LitDoc= document can have one or more specialized source
blocks.

This document, for example, provides a main block. The block
contains a shell script that can be run directly from the
command line using =guix ld run hello=.

#+begin_src shell :tangle main
#! /usr/bin/env python3

 print("Hello World!")

#+end_src

To run the document =python= has to be available locally.
The dependency can be satisified by placing a manifest script
block, which instructs Guix to install python package in an
isolated profile and activate it prior to running the main
script blcok.
#+begin_src scheme :tangle manifest

(specifications->manifest '("python"))

#+end_src

```

To run the document execute,
```sh
guix ld  hello
```
Your terminal should display,
```sh
Hello world!
```

## Documentation

 For more information please *see* https://simfish.dev/projects/litdoc

## Status

 This project started as a collection of loosely related shell scripts meant to simplify
 invoking `guix package` and `guix environment`. The project has since
 evolved. The current plan is to implement the following featuers,
 - **[TODO]** Proper `Guix` package definition for the project instead of
   relying in the current ad-hoc build shell script, `./run`.
   - **[TODO]** Implement `Please.Build` based `Guix` build system.
   - **[TODO]** Implement a `guix ld` command for developing `LitDoc` itself.
 - **[DONE]** Implement a `C/C++` core for parsing `org-mode` document
   and handling specialized source blcoks.
 - **[DONE]** Rewrite the project as an extension to the`guix`command for fine-grained access to
   `GNU/Guix` package and environment management featuers.
## Version History

* 0.1.0

## License

This project is licensed under the GPL-3.0-only or
GPL-3.0-or-later - see the COPYING file for details
